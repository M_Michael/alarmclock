package com.michael.alarmclock.settings

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.PermissionChecker
import com.michael.alarmclock.R
import androidx.appcompat.widget.SwitchCompat
import android.content.Context.MODE_PRIVATE
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.michael.alarmclock.MainActivity
import com.michael.alarmclock.alarm.AlarmsRecyclerFragment
import com.michael.alarmclock.alarm.App


class SettingsFragment : Fragment() {

    private val TAG = "SettingsFragment"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_settings, container, false)

    @SuppressLint("RestrictedApi")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mActivity= activity as MainActivity
        view.findViewById<TextView>(R.id.textView).isSelected = true
        view.findViewById<TextView>(R.id.weather_text).isSelected = true

        val txtToolbar= activity!!.findViewById<TextView>(R.id.text_toolbar)
        txtToolbar.text = "Settings"

        val btnBack= activity!!.findViewById<ImageView>(R.id.action_view)
        btnBack.setImageDrawable(activity!!.resources.getDrawable(R.drawable.ic_skip_icon))
        btnBack.setOnClickListener {
            mActivity.changeFragment(AlarmsRecyclerFragment())
        }

        val sharedPreferences = activity!!.applicationContext.getSharedPreferences(App.SHARED_PREFS, MODE_PRIVATE)
        val editor = sharedPreferences.edit()

        val weatherSwitch = view.findViewById<SwitchCompat>(R.id.switch_weather)
        val switchOnOff = sharedPreferences.getBoolean(App.SWITCH_WEATHER, false)
        weatherSwitch.isChecked = when {
            switchOnOff && checkLocationPermission(false) -> true
            else -> false
        }

        weatherSwitch.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked && checkLocationPermission(true)) {
                Log.i(TAG, "CHECKED")

                weatherSwitch.isChecked = true
                editor.putBoolean(App.SWITCH_WEATHER, true)
                editor.apply()
            } else {
                Log.i(TAG, "UNCHECKED")

                weatherSwitch.isChecked = false
                editor.putBoolean(App.SWITCH_WEATHER, false)
                editor.apply()
            }
        }

        val volumeSwitch = view.findViewById<SwitchCompat>(R.id.switch_disable_volume)
        val volumeDisable = sharedPreferences.getBoolean(App.DISABLE_VOLUME, false)

        volumeSwitch.isChecked = volumeDisable
        volumeSwitch.setOnCheckedChangeListener { _, isChecked ->
            editor.putBoolean(App.DISABLE_VOLUME, isChecked)
            editor.apply()
        }

        //val mActivity = activity as MainActivity
        val format12 = view.findViewById<RadioButton>(R.id.format_12)
        val format24 = view.findViewById<RadioButton>(R.id.format_24)
        when (MainActivity.timeFormat) {
            12 -> {
                format12.isChecked = true
                format24.isChecked = false
            }
            24 -> {
                format12.isChecked = false
                format24.isChecked = true
            }
        }

        val formatType = view.findViewById<RadioGroup>(R.id.format_radio_group)
        formatType.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.format_24 -> {
                    //Log.i(TAG, "btn id 24")
                    editor.putInt(App.TIME_FORMAT, 24)
                    editor.apply()
                }
                R.id.format_12 -> {
                    //Log.i(TAG, "btn id 12")
                    editor.putInt(App.TIME_FORMAT, 12)
                    editor.apply()
                }
            }
        }

        val mFab = activity!!.findViewById<FloatingActionButton>(R.id.add_alarm)
        mFab.visibility = View.INVISIBLE
    }

    private fun checkLocationPermission(showDialog: Boolean): Boolean {
        //var locationPermission = false
        if (Build.VERSION.SDK_INT >= 23) {

            if (PermissionChecker.checkSelfPermission(
                    activity!!.applicationContext,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED) {
                Log.i(TAG, "Permission is granted")
                //locationPermission = true
            } else {
                Log.i(TAG, "Permission is revoked")
                if (showDialog) {
                    AlertDialog.Builder(activity!!)
                        .setTitle("Permission needed")
                        .setMessage("This permission is needed for getting weather in your location.")
                        .setPositiveButton("Ok") { _, _ ->
                            ActivityCompat.requestPermissions(
                                activity!!,
                                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                                1
                            )
                        }
                        .setNegativeButton("Cancel") { dialog, _ ->
                            dialog.dismiss()
                            //locationPermission = false
                        }
                        .create().show()
                }
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.i(TAG, "Permission is granted")
            //locationPermission = true
        }

        return PermissionChecker.checkSelfPermission(
            activity!!.applicationContext,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    }

}