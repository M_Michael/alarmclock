package com.michael.alarmclock.alarm

import android.app.Application
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.michael.alarmclock.alarm.util.addAlarm
import java.util.*

class BootCompletedReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val viewModel = AlarmViewModel(context!!.applicationContext as Application)
        val alarmsList = viewModel.getAlarms()
        lateinit var calendar: Calendar

        for (alarm in alarmsList) {
            if (!alarm.enabled) continue
            calendar = Calendar.getInstance()
            calendar.set(Calendar.HOUR_OF_DAY, alarm.hour)
            calendar.set(Calendar.MINUTE, alarm.min)
            calendar.set(Calendar.SECOND, 0)
            calendar.set(Calendar.MILLISECOND, 0)

            addAlarm(context.applicationContext, calendar, alarm)
        }
    }

}