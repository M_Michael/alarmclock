package com.michael.alarmclock.alarm

import android.app.AlarmManager
import android.app.Application
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import com.michael.alarmclock.data.alarm.Alarm
import androidx.core.content.ContextCompat
import com.michael.alarmclock.alarm.service.NotificationService
import com.michael.alarmclock.alarm.util.setAlarm
import com.google.gson.Gson
import java.lang.Exception


class AlarmReceiver : BroadcastReceiver() {
    private val TAG = "AlarmReceiver"
    private lateinit var alarm: Alarm
    private  var alarmId: Int = 0

    override fun onReceive(context: Context, intent: Intent) {
        Log.i(TAG, "onReceive")

        val viewModel = AlarmViewModel(context.applicationContext as Application)
        //Log.d(TAG, "GET IT: " + intent.getParcelableExtra("123") as Alarm)
        Log.d(TAG, "GET IT")
        alarmId = intent.getIntExtra("alarmId", 0)
        if (alarmId == 0) return
        try {
            alarm = viewModel.getAlarmById(alarmId)
        } catch (e: Exception) {
            //Log.d(TAG, "${e.message}")
            return
        }

        if (intent.action == App.SKIPPED) {
            alarm.allowedSkippingTimes = intent.getIntExtra("skipLeft", 0)
            //Log.d(TAG, "alarmId: ${alarm.id}, skip: ${alarm.allowedSkippingTimes}")
        } else {
            if (!alarm.daysToRepeat.isNullOrEmpty() && alarm.isRepeating) {
                alarm.timeInMillis += AlarmManager.INTERVAL_DAY * 7
                val myIntent = Intent(context, AlarmReceiver::class.java)
                myIntent.putExtra("alarmId", alarm.id)
                val pendingIntent =
                    PendingIntent.getBroadcast(context, alarm.id, myIntent, PendingIntent.FLAG_UPDATE_CURRENT)
                setAlarm(context, alarm.timeInMillis, pendingIntent)
            }
        }

        val serviceIntent = Intent(context, NotificationService::class.java)
        serviceIntent.putExtra("alarm", alarm)
        serviceIntent.putExtra("start", true)
        Log.d(TAG, "")
        ContextCompat.startForegroundService(context, serviceIntent)
    }
}