package com.michael.alarmclock.alarm

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.graphics.Color
import android.opengl.Visibility
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import android.view.animation.LinearInterpolator
import android.widget.TextView
import androidx.appcompat.widget.ViewUtils
import androidx.fragment.app.Fragment
import com.airbnb.lottie.LottieAnimationView
import com.airbnb.lottie.LottieProperty
import com.airbnb.lottie.model.KeyPath
import com.michael.alarmclock.R
import kotlinx.android.synthetic.main.fragment_game.*
import java.util.*
import kotlin.concurrent.schedule
import kotlin.random.Random

class GameFragment: Fragment() {

    private var bubbleAppearDelay: Long = 0
    private var bubbleLifetimeDelay: Long = 0
    private var gameStartTime: Long = 0
    private var isGameInited: Boolean = false
    private val handler = Handler()
    private var bubbleList = arrayListOf<Bubble>()
    private lateinit var mainHandler: Handler
    private var missCount = 0
    private var goalCount = 0
    private val mActivity by lazy { activity as GameStopActivity }
    private val txtGoal by lazy { activity!!.findViewById<TextView>(R.id.goal_text_view) }
    private val txtMiss by lazy { activity!!.findViewById<TextView>(R.id.miss_text_view) }
    private val gameView by lazy { activity!!.findViewById<View>(R.id.game_view) }
    private val turnOfftxt by lazy { activity!!.findViewById<TextView>(R.id.text_turn_off) }
    private val turnOffBtn by lazy { activity!!.findViewById<LottieAnimationView>(R.id.turn_off_button) }
    private val shine by lazy { activity!!.findViewById<LottieAnimationView>(R.id.shine) }
    private val activeBubble = "json/Clock_5.json"
    //private val passiveBubble = "muzli_beacon.json"
    private val passiveBubble = "json/Clock_2.json"
    private var isLost = false
    private var endGameDelay = 0L

    private val updateTextTask = object : Runnable {
        override fun run() {

            //Long running code
            val result = getNextRandView()
            // Use result
            val id = resources.getIdentifier("cell_$result", "id", activity!!.packageName)
            val mView = activity!!.findViewById<LottieAnimationView>(id)
//            mView.addValueCallback(KeyPath("**"), LottieProperty.COLOR) {
//                Color.GREEN
//            }
            mView.setAnimation(activeBubble)
            //mView.scale = 0.2f
            mView.loop(true)
            mView.playAnimation()
            bubbleList[result.toInt() - 1].status = "active"

            handler.postDelayed({
                if (activity != null && bubbleList[result.toInt() - 1].status != "popped") {
                    val id = resources.getIdentifier("cell_$result", "id", activity!!.packageName)
                    val mView = activity!!.findViewById<LottieAnimationView>(id)
//                mView.addValueCallback(
//                    KeyPath("**"),
//                    LottieProperty.COLOR) {
//                    Color.RED
//                }
                    mView.setAnimation(passiveBubble)
                    //mView.scale = 0.2f
                    mView.loop(true)
                    mView.playAnimation()

                    //if (bubbleList[result.toInt() - 1].status != "popped") {
                        Log.d("rand", "${result.toInt() - 1} status: ${bubbleList[result.toInt() - 1].status}")
                    if (bubbleList[result.toInt() - 1].status == "active") {
                        missCount ++
                    }

                    bubbleList[result.toInt() - 1].status = "passive"
                    setText()
                }
            }, bubbleLifetimeDelay)

            if (missCount >= 3) {
                isLost = true
                bubbleLifetimeDelay = 0L
                endGameDelay = 0L
            }

            mainHandler.postDelayed(this, bubbleAppearDelay)
            if (gameStartTime + GameStopActivity.currentAlarm.gameTime < Date().time || isLost) {
                mainHandler.removeCallbacks(this)
                handler.postDelayed({
                    startOutAnimation()
                    mainHandler.removeCallbacks(this)
                    handler.postDelayed({
                        // TODO проверять конец ли игры или нужно на рестарт
                        if (isLost) {
                            GameStopActivity.isRetry = true
                            mActivity.changeFragment("pre_game")
                        } else {
                            GameStopActivity.isWon = true
                            showTurnOffButton()
                        }
                    }, 850)
                }, bubbleLifetimeDelay)
            }
        }
    }

    private fun getNextRandView(): String {
        var rand: Int
        do {
            rand = Random.nextInt(1, 11)
        } while (bubbleList[rand - 1].status != "passive")

        return rand.toString()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_game, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        txtGoal.visibility      = View.INVISIBLE
        txtMiss.visibility      = View.INVISIBLE
        gameView.visibility     = View.INVISIBLE
        turnOfftxt.visibility   = View.INVISIBLE
        turnOffBtn.visibility   = View.INVISIBLE
        shine.visibility        = View.INVISIBLE
        Log.d("GameFragment", "isWon: ${GameStopActivity.isWon}")
        if (GameStopActivity.isWon) {
            showTurnOffButton()
            return
        }

        for (u in 1..10) {
            bubbleList.add(Bubble("passive"))
            val id = resources.getIdentifier("cell_$u", "id", activity!!.packageName)
            val mView = activity!!.findViewById<LottieAnimationView>(id)
            mView.setAnimation(passiveBubble)
            mView.scale = 0.2f
            mView.loop(true)
            mView.playAnimation()

            mView.setOnClickListener {
                mView.isEnabled = false
                if (bubbleList[u - 1].status == "passive")
                    missCount++
                else
                    goalCount++

                bubbleList[u - 1].status = "popped"

                mView.cancelAnimation()
                mView.removeAllAnimatorListeners()
                mView.setAnimation("json/particle_explosion.json")
                mView.loop(false)
                //mView.scale = 0.5f
                //mView.loop(false)

                mView.addAnimatorListener(object : Animator.AnimatorListener {
                    override fun onAnimationRepeat(animation: Animator?) {
                    }

                    override fun onAnimationCancel(animation: Animator?) {
                    }

                    override fun onAnimationStart(animation: Animator?) {
                    }

                    override fun onAnimationEnd(animation: Animator?) {

                        Log.d("Anim", "onAnimationEnd()")
                        mView.setAnimation(passiveBubble)
                        mView.loop(true)
                        mView.playAnimation()
                        mView.isEnabled = true
                        bubbleList[u - 1].status = "passive"
                    }
                })
                mView.playAnimation()
                setText()
            }

        }

        val countdown = activity!!.findViewById<LottieAnimationView>(R.id.countdown)
        countdown.addAnimatorListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) {
            }

            override fun onAnimationCancel(animation: Animator?) {
            }

            override fun onAnimationStart(animation: Animator?) {
            }

            override fun onAnimationEnd(animation: Animator?) {
                countdown.visibility = View.GONE
                Thread.sleep(500)
                initGame()

            }
        })
        countdown.playAnimation()

    }

    private fun initGame() {
        missCount = 0
        goalCount = 0
        gameStartTime = Date().time

        txtGoal.visibility = View.VISIBLE
        txtMiss.visibility = View.VISIBLE
        gameView.visibility = View.VISIBLE

        setText()

        bubbleLifetimeDelay = 2000 / GameStopActivity.level
        bubbleAppearDelay = 1000/ GameStopActivity.level

        isGameInited = true

        mainHandler = Handler(Looper.getMainLooper())
        mainHandler.post(updateTextTask)
    }

    private fun startOutAnimation() {
        for (u in 1..10) {
            bubbleList.add(Bubble("passive"))
            val id = resources.getIdentifier("cell_$u", "id", activity!!.packageName)
            val mView = activity!!.findViewById<LottieAnimationView>(id)
            mView.isEnabled = false

            val startDelay = Random.nextInt(0, 150).toLong()
            val positionAnimator = ValueAnimator.ofFloat(0f, Random.nextInt(130, 230).toFloat())
            positionAnimator.addUpdateListener {
                val value = it.animatedValue as Float
                mView.translationY = value
            }
            positionAnimator.startDelay = startDelay

            val rotationAnimator = ObjectAnimator.ofFloat(mView, "rotation", 0f, 60f)
            rotationAnimator.startDelay = startDelay

            val alphaAnimator = ObjectAnimator.ofFloat(mView, "alpha", 0f)
            alphaAnimator.startDelay = startDelay

            val animatorSet = AnimatorSet()
            animatorSet.interpolator = LinearInterpolator()
            animatorSet.play(positionAnimator).with(rotationAnimator).with(alphaAnimator)
            animatorSet.duration = Random.nextInt(400, 700).toLong()
            animatorSet.start()

            animatorSet.addListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(animation: Animator?) {
                }

                override fun onAnimationCancel(animation: Animator?) {
                }

                override fun onAnimationStart(animation: Animator?) {
                }

                override fun onAnimationEnd(animation: Animator?) {
                    mView.cancelAnimation()
                }
            })
        }

        val valueAnimator = ValueAnimator.ofFloat(0f)

        valueAnimator.addUpdateListener {
            val value = it.animatedValue as Float
            txtGoal.alpha = value
            txtMiss.alpha = value
        }

        valueAnimator.interpolator = AccelerateInterpolator()
        valueAnimator.duration = 150
        valueAnimator.start()

    }

    private fun showTurnOffButton() {

        val positionAnimator = ValueAnimator.ofFloat(-150f, countdown.y - turnOffBtn.width / 2 + 100)
        positionAnimator.addUpdateListener {
            val value = it.animatedValue as Float
            turnOffBtn.translationY = value
            turnOfftxt.translationY = value + 50
        }

        val alphaAnimator = ObjectAnimator.ofFloat(turnOffBtn, "alpha", 0f, 1f)

        val alphaTxtAnimator = ObjectAnimator.ofFloat(turnOfftxt, "alpha", 0f, 1f)

        val animatorTxtSet = AnimatorSet()
        animatorTxtSet.interpolator = LinearInterpolator()
        animatorTxtSet.play(positionAnimator).with(alphaTxtAnimator).with(alphaAnimator)
        animatorTxtSet.duration = 500L
        animatorTxtSet.start()

        turnOffBtn.setOnClickListener {
            GameStopActivity.isDisabled = true
            mActivity.finishActivity()
        }

        shine.playAnimation()
        val alphaShineAnimator = ObjectAnimator.ofFloat(shine, "alpha", 0f, 1f)
        alphaShineAnimator.start()

        shine.visibility      = View.VISIBLE
        turnOffBtn.visibility = View.VISIBLE
        turnOfftxt.visibility = View.VISIBLE
    }

    private fun setText() {
        txtMiss.text = "Miss: $missCount"
        txtGoal.text = "Goal: $goalCount"
    }

    override fun onPause() {
        super.onPause()
        if (isGameInited) {
            mainHandler.removeCallbacks(updateTextTask)
        }
    }

    override fun onResume() {
        super.onResume()
        if (isGameInited) {
            mainHandler.post(updateTextTask)
        }
    }
}