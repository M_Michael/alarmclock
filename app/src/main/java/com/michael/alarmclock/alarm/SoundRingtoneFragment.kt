package com.michael.alarmclock.alarm

import android.database.Cursor
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.fragment.app.Fragment
import com.michael.alarmclock.R
import com.michael.alarmclock.data.Sound
import android.media.RingtoneManager
import android.media.MediaPlayer
import android.net.Uri
import android.provider.MediaStore


class SoundRingtoneFragment : Fragment() {

    lateinit var ringtoneList: ListView
    private val TAG = "SoundRingtoneFragment"
    lateinit var adapter: SoundListAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater!!.inflate(R.layout.fragment_ringtone, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        populateListView(getNotifications())


        //populateListView(sounds(MediaStore.Audio.Media.IS_RINGTONE))
    }

    // Display the specific sound list on list view
    private fun populateListView(list: ArrayList<Sound>) {
        // Get specific sound titles list
        //val titles = arrayListOf<String>()
        //for (music in list){titles.add(music.title)}

        ringtoneList = activity!!.findViewById(R.id.ringtone_list_view)
        var act = activity!!
        adapter = SoundListAdapter(activity!!.applicationContext, R.layout.sound_list_item, list, act, 0)



//        val listItem =  ringtone_list_view.getItemAtPosition(position)
//        Log.d(TAG, "OnClickListener clicked item: " + listItem.toString())
//        Log.d(TAG, "OnClickListener sending uri: " + adapter.getSelectedSound()!!.uri)
//        if (adapter.getSelectedSound()?.uri != null)
//            dataPasser.onDataPass(adapter.getSelectedSound()!!.uri)

        ringtoneList.adapter = adapter
        SoundActivity.ringtoneAdapter = adapter
//        ringtoneList.setOnItemClickListener { parent, view, position, id ->
//            Toast.makeText(activity, "Position Clicked:"+" "+position,Toast.LENGTH_SHORT).show()
//        }

    }

//    // Extension method to get all specific type audio/sound files list
    private fun sounds(): ArrayList<Sound> {
        // Initialize an empty mutable list of sounds
        val list: ArrayList<Sound> = arrayListOf()

        // Get the internal storage media store audio uri
        //val uri: Uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
        val uri: Uri = MediaStore.Audio.Media.INTERNAL_CONTENT_URI

        // Non-zero if the audio file type match
        val selection = MediaStore.Audio.Media.IS_RINGTONE + "!= 0"

        // Sort the audio
        val sortOrder = MediaStore.Audio.Media.TITLE + " ASC"
        //val sortOrder = MediaStore.Audio.Media.TITLE + " DESC"

        // Query the storage for specific type audio files
        val cursor: Cursor = activity!!.applicationContext.contentResolver.query(
            uri, // Uri
            null, // Projection
            selection, // Selection
            null, // Selection arguments
            sortOrder // Sort order
        )

        // If query result is not empty
        if (cursor!= null && cursor.moveToFirst()) {

            val id: Int = cursor.getColumnIndex(MediaStore.Audio.Media._ID)
            val title: Int = cursor.getColumnIndex(MediaStore.Audio.Media.TITLE)
            val titleKey: Int = cursor.getColumnIndex(MediaStore.Audio.Media.TITLE_KEY)

            // Now loop through the audio files
            do {
                val audioId: Long = cursor.getLong(id)
                val audioTitle: String = cursor.getString(title)
                val audioTitleKey: String = cursor.getString(titleKey)
                //val audioType: String = cursor.getString(type)
                //Log.d("AUDIO", "long: ${MediaStore.Audio.Media.EXTERNAL_CONTENT_URI}/$audioTitleKey./$audioType")
                // Add the current audio/sound to the list
                val notificationUri =
                    cursor.getString(RingtoneManager.URI_COLUMN_INDEX) + "/" + cursor.getString(RingtoneManager.ID_COLUMN_INDEX)
                list.add(Sound(audioTitle, notificationUri))
            } while (cursor.moveToNext())
        }

        // Finally, return the audio files list
        return list
    }

    override fun onDestroy() {
//        val resultIntent = Intent()
        Log.d(TAG, "onDestroy")
//        resultIntent.putExtra("soundUri", adapter.getSelectedSound().uri)
//        resultIntent.putExtra("soundTitle", adapter.getSelectedSound().title)
        //activity!!.sound
        //if (adapter.getSelectedSound()?.uri != null)
            //dataPasser.onDataPass(adapter.getSelectedSound()!!.uri)
        super.onDestroy()
    }

    private fun getNotifications(): ArrayList<Sound> {
        val manager = RingtoneManager(activity)
        manager.setType(RingtoneManager.TYPE_RINGTONE)
        val cursor = manager.cursor

        val list: ArrayList<Sound> = arrayListOf()
        while (cursor.moveToNext()) {
            val notificationTitle = cursor.getString(RingtoneManager.TITLE_COLUMN_INDEX)
            val notificationUri =
                cursor.getString(RingtoneManager.URI_COLUMN_INDEX) + "/" + cursor.getString(RingtoneManager.ID_COLUMN_INDEX)

            list.add(Sound(notificationTitle, notificationUri))
        }

        return list
    }
}