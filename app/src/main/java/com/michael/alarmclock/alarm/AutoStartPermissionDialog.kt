package com.michael.alarmclock.alarm

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import com.michael.alarmclock.R
import java.lang.ClassCastException
import android.view.inputmethod.InputMethodManager
import android.widget.CheckBox
import androidx.core.content.ContextCompat.getSystemService
import com.judemanutd.autostarter.AutoStartPermissionHelper


class AutoStartPermissionDialog : DialogFragment() {

    private val TAG = "AutoStartPermDialog"

    interface PermissionListener {
        fun sendInput(permission: Boolean)
    }

    private var mOnPermissionListener: PermissionListener? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var myView = inflater.inflate(R.layout.dialog_autostart_permission, container, false)

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        setStyle(STYLE_NO_FRAME, 0)
        super.onCreateView(inflater, container, savedInstanceState)

        val permissionCheckBox = myView.findViewById<CheckBox>(R.id.permission_checkbox)
        val btnPermissions = myView.findViewById<Button>(R.id.button_open_permissions)
        val btnDone = myView.findViewById<Button>(R.id.button_permission_done)

        btnDone.setOnClickListener{
            mOnPermissionListener!!.sendInput(permissionCheckBox.isChecked)
            dismiss()
        }

        btnPermissions.setOnClickListener{
            AutoStartPermissionHelper.getInstance().getAutoStartPermission(activity!!)
        }

        return myView
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            mOnPermissionListener = activity as PermissionListener?
        } catch (e: ClassCastException) {
            Log.e(TAG, "onAttach: ClassCastException: " + e.message)
        }

    }
}