package com.michael.alarmclock.alarm.util

import java.lang.StringBuilder
import java.util.*
import java.util.concurrent.TimeUnit

fun calcTimeLeft(futureTime: Long, isRepeating: Boolean = false) : String {
    var diffMilliseconds = futureTime - Date().time
    if (!isRepeating && diffMilliseconds < 0)
        diffMilliseconds += 86400000
    var diffSeconds = TimeUnit.SECONDS.convert(diffMilliseconds, TimeUnit.MILLISECONDS)

    fun calculateTime(seconds: Int): Int {
        var measurement = diffSeconds / seconds
        diffSeconds -= measurement * seconds
        return measurement.toInt()
    }

    val days = calculateTime(86400)
    val hours = calculateTime(3600)
    val minutes = calculateTime(60)
    //val seconds = diffSeconds.toInt()

    //var strSec = " $seconds sec"
    var strMin = ""
    var strHr = ""
    var strDays = ""

    var message = StringBuilder()

    if (days > 0) strDays = " $days days"
    if (hours > 0 || days > 0) strHr = " $hours hours"
    if (minutes > 0 || hours > 0 || days > 0) strMin = " $minutes min"

    if (minutes <= 0 && hours == 0 && days == 0) {
        message.append("Alarm will ring in less than a minute")
    } else {
        message.append("Alarm will ring in")
            .append(strDays)
            .append(strHr)
            .append(strMin)
            //.append(strSec)
    }
    return message.toString()
}