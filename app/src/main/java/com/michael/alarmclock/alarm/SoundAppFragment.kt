package com.michael.alarmclock.alarm

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.fragment.app.Fragment
import com.michael.alarmclock.R
import com.michael.alarmclock.data.Sound
import com.michael.alarmclock.data.getAppSounds


/**
 * A simple [Fragment] subclass.
 */
class SoundAppFragment : Fragment() {

    private val TAG = "SoundAppFragment"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_app_music, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        populateListView(getAppSounds())

    }

    private fun populateListView(list: ArrayList<Sound>) {
        val ringtoneList = activity!!.findViewById<ListView>(R.id.app_music_list_view)
        val adapter = SoundListAdapter(activity!!.applicationContext, R.layout.sound_list_item, list, activity!!, 1)

        ringtoneList.adapter = adapter
        SoundActivity.appMusicAdapter = adapter
    }

    override fun onDestroy() {
        Log.d(TAG, "onDestroy")
        super.onDestroy()
    }
}