//package com.michael.alarmclock.alarm
//
//import android.app.Activity
//import android.content.Context
//import android.content.Intent
//import android.os.Build
//import android.os.Bundle
//import android.util.Log
//import androidx.appcompat.app.AppCompatActivity
//import android.view.View
//import android.view.Window
//import android.view.animation.Animation
//import android.view.animation.BounceInterpolator
//import android.view.animation.ScaleAnimation
//import android.widget.*
//import androidx.appcompat.widget.SwitchCompat
//import androidx.constraintlayout.widget.ConstraintLayout
//import androidx.lifecycle.ViewModelProviders
//import com.michael.alarmclock.R
//import com.michael.alarmclock.alarm.util.*
//import com.michael.alarmclock.data.alarm.Alarm
////import com.michael.alarmclock.data.alarm.ShutdownType
//import kotlinx.android.synthetic.main.daypicker.*
//import java.util.*
//import org.jetbrains.anko.toast
//import android.provider.OpenableColumns
//import android.net.Uri
//import android.provider.MediaStore
//import android.view.MenuItem
//import com.michael.alarmclock.data.getAppSoundNameByUri
//
//
//
//class AlarmActivity : AppCompatActivity(), AlarmDescriptionDialog.OnInputListener {
//
//    private val TAG = "AlarmActivity"
//    private val ShutdownActivityCode = 0
//    private val SoundActivityCode = 1
//
//    override fun sendInput(input: String?) {
//        //Log.i(TAG, "sendInput(): got the input: $input")
//        description = input
//        descText.text = getDescription(description)
//    }
//
//    private lateinit var currentAlarm: Alarm
//
//    private val mAlarmViewModel     by lazy { ViewModelProviders.of(this)[AlarmViewModel::class.java] }
//    private val timePicker          by lazy { findViewById<TimePicker>(R.id.time_picker) }
//    private val context             by lazy { this }
//    private val btnDesc             by lazy { findViewById<Button>(R.id.button_alarm_description) }
//    private val btnShutdownType     by lazy { findViewById<Button>(R.id.button_shutdown_type) }
//    private val btnSet              by lazy { findViewById<Button>(R.id.button_alarm_description) }
//    private val btnDelete           by lazy { findViewById<Button>(R.id.button_alarm_description) }
//    private val descText            by lazy { findViewById<TextView>(R.id.textview_alarm_description) }
//    private val shutdownTypeText    by lazy { findViewById<TextView>(R.id.textview_alarm_shutdown_type) }
//    private val soundText           by lazy { findViewById<TextView>(R.id.textview_alarm_sound) }
//    private val btnSound            by lazy { findViewById<Button>(R.id.button_sound) }
//    private val volumeBar           by lazy { findViewById<SeekBar>(R.id.volume_level) }
//    private val vibrationSwitcher   by lazy { findViewById<SwitchCompat>(R.id.vibration_switcher) }
//    private val calendar: Calendar = Calendar.getInstance()
//    private var sound: String? = ""
//    private var description: String? = ""
//    private var hour = 0
//    private var min = 0
//    private var isDefault = true
//    private var isShake = false
//    private var shakeCount = 0
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        //supportActionBar!!.setDisplayHomeAsUpEnabled(true)
//        setContentView(R.layout.alarm_edit_layout)
//
//        //this.setFinishOnTouchOutside(true)
//
//        shutdownTypeText.text = "Default"
//        soundText.text = printSoundTitle("")
//
//        timePicker.setIs24HourView(true)
//
//        if (intent.extras != null) {
//            currentAlarm = mAlarmViewModel.getAlarmById(intent.getIntExtra("alarm_id", 0))
////            if (currentAlarm != null) {
//                //Log.i(TAG, "Alarm enum: ${currentAlarm.shutdownType}")
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                    timePicker.hour = currentAlarm.hour
//                    timePicker.minute = currentAlarm.min
//                } else {
//                    timePicker.currentHour = currentAlarm.hour
//                    timePicker.currentMinute = currentAlarm.min
//                }
//            //}
//
//            if(currentAlarm.daysToRepeat.indexOf(1) != -1) day_Sn.isChecked = true
//            if(currentAlarm.daysToRepeat.indexOf(2) != -1) day_Mn.isChecked = true
//            if(currentAlarm.daysToRepeat.indexOf(3) != -1) day_Tu.isChecked = true
//            if(currentAlarm.daysToRepeat.indexOf(4) != -1) day_Wd.isChecked = true
//            if(currentAlarm.daysToRepeat.indexOf(5) != -1) day_Th.isChecked = true
//            if(currentAlarm.daysToRepeat.indexOf(6) != -1) day_Fr.isChecked = true
//            if(currentAlarm.daysToRepeat.indexOf(7) != -1) day_Sa.isChecked = true
//
//            description = currentAlarm.description
//            descText.text = getDescription(description)
//            this.isDefault = currentAlarm.isShutdownDefault
//            this.isShake = currentAlarm.isShutdownShake
//            this.shakeCount = currentAlarm.shakeCount
//            this.sound = currentAlarm.sound
//            volumeBar.progress = currentAlarm.volume
//            vibrationSwitcher.isChecked = currentAlarm.vibration
//            shutdownTypeText.text = printShutdownType()
//            soundText.text = printSoundTitle(currentAlarm.sound)
//        }
//
//        val textTimeLeft = findViewById<TextView>(R.id.text_time_left)
//        initCalendar()
//        textTimeLeft.text = calcTimeLeft(calendar.timeInMillis)
//
//        timePicker.setOnTimeChangedListener { view, hourOfDay, minute ->
//            initCalendar()
//            textTimeLeft.text = calcTimeLeft(calendar.timeInMillis)
//        }
//
//        btnDesc.setOnClickListener{
//                val alarmDialog = AlarmDescriptionDialog()
//                val fm = supportFragmentManager
//                alarmDialog.show(fm, "MyDialog")
//        }
//
//        btnShutdownType.setOnClickListener {
//            val intent = Intent(this, ShutdownTypeActivity::class.java)
//            intent.putExtra("isDefault", isDefault)
//            intent.putExtra("isShake", isShake)
//            intent.putExtra("shakeCount", shakeCount)
//            startActivityForResult(intent, ShutdownActivityCode)
//        }
//
//        btnSound.setOnClickListener {
//            val intent = Intent(this, SoundActivity::class.java)
//            intent.putExtra("sound", sound)
//            startActivityForResult(intent, SoundActivityCode)
//        }
//
//        btnDelete.setOnClickListener {
//            mAlarmViewModel.deleteAlarm(currentAlarm)
//            deleteAlarm(context.applicationContext, currentAlarm)
//            this@AlarmActivity.finish()
//        }
//// Animation block
////        var scaleAnimation = ScaleAnimation(0.7f, 1.0f, 0.7f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
////        scaleAnimation?.setDuration(500)
////        var bounceInterpolator = BounceInterpolator()
////        scaleAnimation?.setInterpolator(bounceInterpolator)
////
////        val dayButtonArray = arrayListOf<ToggleButton>(day_Mn, day_Tu, day_Wd,
////            day_Th, day_Fr, day_Sa, day_Sn)
////  Тут проблема с анимациями. Когда анимаия предыдущего нажатия не зкончилась и кликнуть в это время на другую кнопку, то анимация повторится для двух кнопок.
////        for (day_btn in dayButtonArray) {
////            day_btn.setOnCheckedChangeListener(object : View.OnClickListener, CompoundButton.OnCheckedChangeListener {
////                override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
////                    p0?.startAnimation(scaleAnimation)
////                    Log.d(
////                        "fav",
////                        "am i here"
////                    ) //To change body of created functions use File | Settings | File Templates.
////                }
////
////                override fun onClick(p0: View?) {
////
////                }
////            })
////        }
//
//        if (!::currentAlarm.isInitialized) {
//            btnDelete.visibility = View.INVISIBLE
//            val params = btnSet.layoutParams as ConstraintLayout.LayoutParams
//            params.startToStart = ConstraintLayout.LayoutParams.PARENT_ID
//            btnSet.requestLayout()
//        }
//        btnSet.setOnClickListener {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                calendar.set(Calendar.HOUR_OF_DAY, timePicker.hour)
//                calendar.set(Calendar.MINUTE, timePicker.minute)
//                calendar.set(Calendar.SECOND, 0)
//                calendar.set(Calendar.MILLISECOND, 0)
//                hour = timePicker.hour
//                min = timePicker.minute
//            } else {
//                calendar.set(Calendar.HOUR_OF_DAY, timePicker.currentHour)
//                calendar.set(Calendar.MINUTE, timePicker.currentMinute)
//                calendar.set(Calendar.SECOND, 0)
//                calendar.set(Calendar.MILLISECOND, 0)
//                hour = timePicker.currentHour
//                min = timePicker.currentMinute
//            }
//            initCalendar()
//
//            val daysList = arrayListOf<Int>()
//            if(day_Sn.isChecked) daysList.add(Calendar.SUNDAY)
//            if(day_Mn.isChecked) daysList.add(Calendar.MONDAY)
//            if(day_Tu.isChecked) daysList.add(Calendar.TUESDAY)
//            if(day_Wd.isChecked) daysList.add(Calendar.WEDNESDAY)
//            if(day_Th.isChecked) daysList.add(Calendar.THURSDAY)
//            if(day_Fr.isChecked) daysList.add(Calendar.FRIDAY)
//            if(day_Sa.isChecked) daysList.add(Calendar.SATURDAY)
//            var repeat = false
//            if (!daysList.isNullOrEmpty()) repeat = true
//
//            toast(calcTimeLeft(calendar.timeInMillis))
////            var _alarm = Alarm(
////                isRepeating = repeat, daysToRepeat = daysList, description = description,
////                isShutdownDefault = isDefault, isShutdownShake = isShake, shakeCount = shakeCount
////            )
//            if (::currentAlarm.isInitialized) {
//
//                deleteAlarm(context, currentAlarm)
//                //mAlarmViewModel.deleteAlarm(currentAlarm)
//                currentAlarm.hour = hour
//                currentAlarm.min = min
//                currentAlarm.daysToRepeat = daysList
//                currentAlarm.isRepeating = repeat
//                currentAlarm.description = description
//                currentAlarm.enabled = true
//                currentAlarm.sound = sound
//                currentAlarm.volume = volumeBar.progress
//                currentAlarm.vibration = vibrationSwitcher.isChecked
//                currentAlarm.isShutdownDefault = isDefault
//                currentAlarm.isShutdownShake = isShake
//                currentAlarm.shakeCount = shakeCount
//
//                Log.d(TAG, "send alarm to add new: $currentAlarm")
//                val id = addAlarm(context, calendar, currentAlarm)
//                currentAlarm.id = id
//                mAlarmViewModel.insert(currentAlarm)
//            } else {
//                val _alarm = Alarm(
//                    0, min, hour, calendar.timeInMillis, true, daysList, repeat,
//                    description = description, isShutdownDefault = isDefault,
//                    isShutdownShake = isShake, shakeCount = shakeCount, sound = sound,
//                    volume = volumeBar.progress, vibration = vibrationSwitcher.isChecked
//                )
//                val id = addAlarm(context, calendar, _alarm)
//                _alarm.id = id
//                mAlarmViewModel.insert(_alarm)
//            }
//            this@AlarmActivity.finish()
//        }
//    }
//
//    override fun finish() {
//        super.finish()
//        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
//    }
//
//    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        when (requestCode) {
//            ShutdownActivityCode -> {
//                if (resultCode == Activity.RESULT_OK) {
//                    val isDefault = data!!.getBooleanExtra("isDefault", true)
//                    val isShake = data.getBooleanExtra("isShake", true)
//                    val shakeCount = data.getIntExtra("shakeCount", 0)
//                    if (::currentAlarm.isInitialized) {
//                        currentAlarm.isShutdownDefault = isDefault
//                        currentAlarm.isShutdownShake = isShake
//                        currentAlarm.shakeCount = shakeCount
//                        mAlarmViewModel.updateAlarm(currentAlarm)
//                    }
//                    this.isDefault = isDefault
//                    this.isShake = isShake
//                    this.shakeCount = shakeCount
//                    shutdownTypeText.text = printShutdownType()
//                }
//            }
//            SoundActivityCode -> {
//                if (resultCode == Activity.RESULT_OK) {
//                    val sound = data!!.getStringExtra("soundUri")
//                    Log.d(TAG, "Get sound uri: $sound")
//                    if (::currentAlarm.isInitialized) {
//                        currentAlarm.sound = sound
//                        mAlarmViewModel.updateAlarm(currentAlarm)
//                    }
//                    Log.d(TAG, sound)
//                    this.sound = sound
//                }
//                soundText.text = printSoundTitle(sound)
//            }
//        }
//    }
//
//    private fun getDescription(string: String?) =
//        if (string.isNullOrEmpty()) "No title" else string
//
//    private fun initCalendar() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            calendar.set(Calendar.HOUR_OF_DAY, timePicker.hour)
//            calendar.set(Calendar.MINUTE, timePicker.minute)
//            calendar.set(Calendar.SECOND, 0)
//            calendar.set(Calendar.MILLISECOND, 0)
//
//            hour = timePicker.hour
//            min = timePicker.minute
//        } else {
//            calendar.set(Calendar.HOUR_OF_DAY, timePicker.currentHour)
//            calendar.set(Calendar.MINUTE, timePicker.currentMinute)
//            calendar.set(Calendar.SECOND, 0)
//            calendar.set(Calendar.MILLISECOND, 0)
//            hour = timePicker.currentHour
//            min = timePicker.currentMinute
//        }
//
//    }
//
//    private fun printShutdownType(): String {
//        val listTypes = arrayListOf<String>()
//        if (isDefault) listTypes.add("Default")
//        if (isShake && shakeCount > 0) listTypes.add("Shakes: $shakeCount")
//        return listTypes.joinToString()
//    }
//
//    private fun printSoundTitle(soundUri: String?): String {
//
//        if (soundUri.isNullOrBlank()) return "Default"
//        if (!getAppSoundNameByUri(soundUri).isNullOrBlank()) return getAppSoundNameByUri(soundUri)!!
//        val cursor = contentResolver.query(Uri.parse(soundUri), null, null, null, null)
//        val titleIndex = cursor.getColumnIndex(MediaStore.Audio.Media.TITLE)
//        cursor!!.moveToFirst()
//        //val audioTitle: String = cursor.getString(titleIndex)
//        //cursor!!.moveToFirst()
//        return cursor.getString(titleIndex)
//    }
//
//    override fun onDestroy() {
//        Log.d(TAG, "onDestroy()")
//        super.onDestroy()
//    }
//
//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        when (item.itemId) {
//            android.R.id.home -> this@AlarmActivity.finish()
//        }
//        return true
//    }
//}