package com.michael.alarmclock.alarm

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Button
import androidx.fragment.app.DialogFragment
import com.michael.alarmclock.R
import java.lang.ClassCastException
import android.widget.RadioButton
import android.widget.RadioGroup


class AlarmSkipDialog(
    val skipTimes: Int? = null,
    val delayTime: Long? = null) : DialogFragment() {

    var description: String? = ""
    lateinit var btnOK: Button
    lateinit var btnBack: Button
    lateinit var radioGroupTime: RadioGroup
    lateinit var radioGroupTimes: RadioGroup

    private val TAG = "AlarmSkipDialog"

    interface OnSkipResultListener {
        fun sendSkipResult(input: ArrayList<Int>)
    }

    var mOnSkipResultListener: OnSkipResultListener? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var myView = inflater.inflate(R.layout.dialog_alarm_skipping, container, false)

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        setStyle(STYLE_NO_FRAME, 0)
        super.onCreateView(inflater, container, savedInstanceState)

        btnOK = myView.findViewById(R.id.button_ok)
        btnBack = myView.findViewById(R.id.button_back)
        radioGroupTime = myView.findViewById(R.id.radio_group_time)
        radioGroupTimes = myView.findViewById(R.id.radio_group_times)


        // TODO add @checked@ to skipTimes and delayTime radios
        when (skipTimes) {
            1       -> myView.findViewById<RadioButton>(R.id.radio_skipping_times_1).isChecked = true
            2       -> myView.findViewById<RadioButton>(R.id.radio_skipping_times_2).isChecked = true
            3       -> myView.findViewById<RadioButton>(R.id.radio_skipping_times_3).isChecked = true
            4       -> myView.findViewById<RadioButton>(R.id.radio_skipping_times_4).isChecked = true
            5       -> myView.findViewById<RadioButton>(R.id.radio_skipping_times_5).isChecked = true
            else    -> myView.findViewById<RadioButton>(R.id.radio_skipping_times_0).isChecked = true
        }

        when (delayTime?.div(60000)) {
            2L      -> myView.findViewById<RadioButton>(R.id.radio_skipping_time_2).isChecked = true
            3L      -> myView.findViewById<RadioButton>(R.id.radio_skipping_time_3).isChecked = true
            5L      -> myView.findViewById<RadioButton>(R.id.radio_skipping_time_5).isChecked = true
            10L     -> myView.findViewById<RadioButton>(R.id.radio_skipping_time_10).isChecked = true
            15L     -> myView.findViewById<RadioButton>(R.id.radio_skipping_time_15).isChecked = true
            else    -> myView.findViewById<RadioButton>(R.id.radio_skipping_time_1).isChecked = true
        }

        btnOK.setOnClickListener{

            val times =
                when (radioGroupTimes.checkedRadioButtonId) {
                    (R.id.radio_skipping_times_1) -> 1
                    (R.id.radio_skipping_times_2) -> 2
                    (R.id.radio_skipping_times_3) -> 3
                    (R.id.radio_skipping_times_4) -> 4
                    (R.id.radio_skipping_times_5) -> 5
                    else -> 0
                }
            val time =
                when (radioGroupTime.checkedRadioButtonId) {
                    (R.id.radio_skipping_time_1) -> 1
                    (R.id.radio_skipping_time_2) -> 2
                    (R.id.radio_skipping_time_3) -> 3
                    (R.id.radio_skipping_time_5) -> 5
                    (R.id.radio_skipping_time_10) -> 10
                    (R.id.radio_skipping_time_15) -> 15
                    else -> 1
                }
            val list = arrayListOf<Int>()
            list.add(times)
            list.add(time * 60000)
            mOnSkipResultListener!!.sendSkipResult(list)
            dismiss()
        }

        btnBack.setOnClickListener{
            dismiss()
        }


        return myView
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            Log.i(TAG, "onAttach() $parentFragment")
            mOnSkipResultListener = parentFragment as OnSkipResultListener?
        } catch (e: ClassCastException) {
            Log.e(TAG, "onAttach: ClassCastException: " + e.message)
        }

    }
}