package com.michael.alarmclock.alarm

import android.content.Context
import android.media.MediaPlayer
import androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior.setTag
import android.content.Context.LAYOUT_INFLATER_SERVICE
import androidx.core.content.ContextCompat.getSystemService
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.michael.alarmclock.data.Sound
import com.michael.alarmclock.R
import android.R.attr.label
import android.net.Uri
import android.util.Log
import androidx.appcompat.widget.AppCompatRadioButton
import androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior.setTag
import kotlinx.android.synthetic.main.sound_list_item.view.*
import androidx.core.widget.CompoundButtonCompat.setButtonTintList
import android.content.res.ColorStateList
import android.os.Build
import android.graphics.Color
import androidx.fragment.app.FragmentActivity
import java.lang.ClassCastException
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.core.widget.CompoundButtonCompat


class SoundListAdapter constructor(
    private val context: Context,
    private val layout: Int,
    private val arrayList: ArrayList<Sound>,
    private val activity: FragmentActivity,
    private val fragmentId: Int
) : BaseAdapter() {

    private var selectedPosition = -1
    private var currentListened = -1
    private var previousListened = -1
    private var TAG = "SoundListAdapter"
    private val mSendSoundUri: SendSoundUri = activity as SendSoundUri

    interface SendSoundUri {
        fun sendSoundUri(input: String?)
    }

    override fun getCount(): Int {
        return arrayList.size
    }

    override fun getItem(i: Int): Any {
        return ""
    }

    override fun getItemId(i: Int): Long {
        return i.toLong()
    }

    private inner class ViewHolder {
        internal var radioButton: RadioButton? = null
        internal var txtName: TextView? = null
        internal var ivPlay: ImageView? = null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        when (fragmentId) {
            0 -> currentListened = SoundActivity.currentPlayingRingtoneMusic
            1 -> currentListened = SoundActivity.currentPlayingAppMusic
            2 -> currentListened = SoundActivity.currentPlayingMyMusic
        }

        var convertView = convertView
        val viewHolder: ViewHolder
        if (convertView == null) {
            viewHolder = ViewHolder()
            val layoutInflater = context.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater

            convertView = layoutInflater.inflate(layout, null)
            viewHolder.txtName = convertView!!.findViewById(R.id.sound_name)
            val rb = convertView.findViewById<RadioButton>(R.id.sound_radiobutton)

            val colorStateList = ColorStateList(
                arrayOf(intArrayOf(-android.R.attr.state_checked), intArrayOf(android.R.attr.state_checked)),
                intArrayOf(

                    Color.GRAY, ContextCompat.getColor(context, R.color.colorPrimary)
                )
            )
            setButtonTintList(rb, colorStateList)
            viewHolder.radioButton = rb
            viewHolder.ivPlay = convertView.findViewById(R.id.ivPlay) as ImageView

            convertView.tag = viewHolder
        } else {
            viewHolder = convertView.tag as ViewHolder
        }

        //check the radio button if both position and selectedPosition matches
        viewHolder.radioButton?.isChecked = (position === selectedPosition)
        //Set the position tag to both radio button and label
        viewHolder.radioButton?.tag = position
        viewHolder.ivPlay?.tag = position
        viewHolder.txtName?.tag = position

        viewHolder.radioButton?.setOnClickListener { v -> itemCheckChanged(v) }

        viewHolder.txtName?.setOnClickListener { v -> itemCheckChanged(v) }

        val music = arrayList[position]

        viewHolder.txtName!!.text = music.title
        if (currentListened == position && SoundActivity.mMediaPlayer.isPlaying) {
            viewHolder.ivPlay!!.setImageResource(R.drawable.ic_stop_black)
        } else {
            viewHolder.ivPlay!!.setImageResource(R.drawable.ic_play_arrow_black)
            //Log.d(TAG, "resetting values for image in fragment: $fragmentId")
        }
        // play music
        viewHolder.ivPlay!!.setOnClickListener {

//            if (currentListened == position && SoundActivity.mMediaPlayer.isPlaying) {
//                viewHolder.ivPlay!!.setImageResource(R.drawable.ic_stop_black)
//            } else {
//                viewHolder.ivPlay!!.setImageResource(R.drawable.ic_play_arrow_black)
//            }
            Log.d(TAG, "currentListened before init: $currentListened")
            previousListened = currentListened
            currentListened = it.tag as Int
            when (fragmentId) {
                0 -> SoundActivity.currentPlayingRingtoneMusic =  currentListened
                1 -> SoundActivity.currentPlayingAppMusic =  currentListened
                2 -> SoundActivity.currentPlayingMyMusic =  currentListened
            }
            //Log.d(TAG, "currentListened: $currentListened")

            fun playMusic() {
                Log.d(TAG, "URI: " + music.uri)
                //SoundActivity.mMediaPlayer.release()
                //SoundActivity.mMediaPlayer.reset()
                try {
                    SoundActivity.mMediaPlayer.reset()
                    SoundActivity.mMediaPlayer.setDataSource(context, Uri.parse(music.uri))
                    SoundActivity.mMediaPlayer.prepare()
                    SoundActivity.mMediaPlayer.start()
                    Log.d(TAG, "MP started")
                    SoundActivity.currentUsingFragment = fragmentId
                    SoundActivity.updateAdapter()
                } catch (e: Exception) {
                    Toast.makeText(context, "Can't play this music.", Toast.LENGTH_SHORT).show()
                    e.printStackTrace()
                }
            }

            if (SoundActivity.mMediaPlayer.isPlaying) {
                Log.d(TAG, "MP stopped")
                SoundActivity.mMediaPlayer.stop()
                //SoundActivity.previousFragment = -1
                viewHolder.ivPlay!!.setImageResource(R.drawable.ic_play_arrow_black)
                if (previousListened != currentListened) {
                    viewHolder.ivPlay!!.setImageResource(R.drawable.ic_stop_black)
                    playMusic()
                    notifyDataSetChanged()
                }
            } else {
                viewHolder.ivPlay!!.setImageResource(R.drawable.ic_stop_black)
                playMusic()
                notifyDataSetChanged()
            }
        }
        notifyDataSetChanged()
        return convertView
    }

    private fun getSelectedSound(): Sound? {
        Log.d(TAG, "sending selectedPosition: $selectedPosition")
        if (selectedPosition == -1)
            return null
        return arrayList[selectedPosition]
    }

    private fun itemCheckChanged(v: View) {
        selectedPosition = v.tag as Int
        mSendSoundUri.sendSoundUri(getSelectedSound()?.uri)
        Log.d(TAG, "selectedPosition changed to: $selectedPosition")
        notifyDataSetChanged()
    }
}