package com.michael.alarmclock.alarm

import android.Manifest
import android.content.pm.PackageManager
import android.database.Cursor
import android.media.MediaPlayer
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.core.app.ActivityCompat
import androidx.core.content.PermissionChecker
import androidx.fragment.app.Fragment
import com.michael.alarmclock.R
import com.michael.alarmclock.data.Sound
import kotlinx.android.synthetic.main.fragment_my_music.*

/**
 * A simple [Fragment] subclass.
 */
class SoundMyMusicFragment : Fragment() {

    private val TAG = "SoundMyMusicFragment"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_my_music, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (SoundActivity.isStoragePermission) {
            if (sounds().isNullOrEmpty()) my_music_empty_text.text = "Application can't find a song. :("
            else populateListView(sounds())
        } else {
            my_music_empty_text.text = "Application has no permission to load your music. :(\n "
        }
    }

    private fun populateListView(list: ArrayList<Sound>) {

        val ringtoneList = activity!!.findViewById<ListView>(R.id.my_music_list_view)
        val adapter = SoundListAdapter(activity!!.applicationContext, R.layout.sound_list_item, list, activity!!, 2)

        SoundActivity.myMusicAdapter = adapter
        ringtoneList.adapter = adapter
        Log.d(TAG, "myMusicAdapter inited")

    }

    //    // Extension method to get all specific type audio/sound files list
    private fun sounds(): ArrayList<Sound> {
        // Initialize an empty mutable list of sounds
        val list: ArrayList<Sound> = arrayListOf()

        // Get the internal storage media store audio uri
        val uri: Uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
        //val uri: Uri = MediaStore.Audio.Media.INTERNAL_CONTENT_URI

        // Non-zero if the audio file type match
        val selection = MediaStore.Audio.Media.IS_MUSIC + "!= 0"

        // Sort the audio
        val sortOrder = MediaStore.Audio.Media.TITLE + " ASC"
        //val sortOrder = MediaStore.Audio.Media.TITLE + " DESC"

        // Query the storage for specific type audio files
        val cursor: Cursor = activity!!.applicationContext.contentResolver.query(
            uri, // Uri
            null, // Projection
            selection, // Selection
            null, // Selection arguments
            sortOrder // Sort order
        )

        // If query result is not empty
        if (cursor!= null && cursor.moveToFirst()) {

            val id: Int = cursor.getColumnIndex(MediaStore.Audio.Media._ID)
            val title: Int = cursor.getColumnIndex(MediaStore.Audio.Media.TITLE)

            // Now loop through the audio files
            do {
                val audioId: Long = cursor.getLong(id)
                val audioTitle: String = cursor.getString(title)

                // Add the current audio/sound to the list

                val notificationUri = "$uri/$audioId"
                //Log.d(TAG, "uri: $notificationUri")
                list.add(Sound(audioTitle, notificationUri))
            } while (cursor.moveToNext())
        }

        // Finally, return the audio files list
        return list
    }

    override fun onDestroy() {
        Log.d(TAG, "onDestroy")
        super.onDestroy()
    }
}