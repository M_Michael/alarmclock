package com.michael.alarmclock.alarm

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.SwitchCompat
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.michael.alarmclock.MainActivity
import com.michael.alarmclock.R
import com.michael.alarmclock.alarm.util.addAlarm
import com.michael.alarmclock.alarm.util.calcTimeLeft
import com.michael.alarmclock.alarm.util.deleteAlarm
import com.michael.alarmclock.data.alarm.Alarm
import com.michael.alarmclock.data.getAppSoundNameByUri
import kotlinx.android.synthetic.main.daypicker.*
import org.jetbrains.anko.toast
import java.util.*


class AlarmEditFragment(currentAlarmId: Int = 0): Fragment(), AlarmDescriptionDialog.OnInputListener, AlarmSkipDialog.OnSkipResultListener {

    private lateinit var currentAlarm: Alarm
    private var currentAlarmId: Int = currentAlarmId
    private var sound: String? = ""
    private var description: String? = ""
    private var skippingTimes: Int = 0
    private var skippingDelayTime: Long = 0
    private var hour = 0
    private var min = 0
    private val SHUTDOWN_CODE = 0
    private val SOUND_CODE = 1
    private val TAG = "AlarmEditFragment"
    private var isDefault = true
    private var isShake = false
    private var shakeCount = 0
    private var isGame = false
    private var gameLevel = 0
    private var gameTime = 0L
    private lateinit var descText: TextView
    private val shutdownTypeText    by lazy { view!!.findViewById<TextView>(R.id.textview_alarm_shutdown_type) }
    private val soundText           by lazy { view!!.findViewById<TextView>(R.id.textview_alarm_sound) }
    private val volumeBar           by lazy { view!!.findViewById<SeekBar>(R.id.volume_level) }
    private val vibrationSwitcher   by lazy { view!!.findViewById<SwitchCompat>(R.id.vibration_switcher) }
    private val skipText            by lazy { view!!.findViewById<TextView>(R.id.textview_skip) }
    private val hourPicker          by lazy { view!!.findViewById<com.shawnlin.numberpicker.NumberPicker>(R.id.hour_picker) }
    private val minPicker           by lazy { view!!.findViewById<com.shawnlin.numberpicker.NumberPicker>(R.id.min_picker) }
    private val formatPicker        by lazy { view!!.findViewById<com.shawnlin.numberpicker.NumberPicker>(R.id.format_picker) }
    private val mAlarmViewModel     by lazy { ViewModelProviders.of(this)[AlarmViewModel::class.java] }
    private val mActivity           by lazy { activity as MainActivity}
    private var calendar = Calendar.getInstance()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
//        if (savedInstanceState != null) {
//            Log.d(TAG, "savedInstanceState min: ${savedInstanceState.getInt("min")}")
//            Log.d(TAG, "savedInstanceState: ${savedInstanceState.getInt("currentAlarmId")}")
//            if (savedInstanceState.getInt("currentAlarmId") != 0)
//                currentAlarmId = savedInstanceState.getInt("currentAlarmId")
//        }
        return inflater.inflate(R.layout.fragment_alarm_edit, container, false)
    }

    @SuppressLint("RestrictedApi")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //Log.d(TAG, "1111 currentAlarmId: $currentAlarmId")
        descText = view!!.findViewById(R.id.textview_alarm_description)
        val txtToolbar    = activity!!.findViewById<TextView>(R.id.text_toolbar)
        if (currentAlarmId == 0)
            txtToolbar.text = "Add alarm"
        else
            txtToolbar.text = "Edit alarm"

        /**
         * custom block
         */
        val textTimeLeft = view.findViewById<TextView>(R.id.text_time_left)

        val typeface = ResourcesCompat.getFont(activity!!, R.font.nunito_bold)
        hourPicker.typeface = typeface
        minPicker.typeface = typeface
        formatPicker.typeface = typeface

        val twelveHoursFormat = arrayListOf<String>()
        for (num in 1..12) {
            twelveHoursFormat.add(num.toString())
        }

        val twentyFourHoursFormat = arrayListOf<String>()
        for (num in 1..24) {
            if (num < 10)
                twentyFourHoursFormat.add("0$num")
            else if (num == 24)
                twentyFourHoursFormat.add("00")
            else
                twentyFourHoursFormat.add(num.toString())
        }

        hourPicker.minValue = 1
        if (MainActivity.timeFormat == 12) {
            hourPicker.maxValue = twelveHoursFormat.size
            hourPicker.displayedValues = twelveHoursFormat.toArray(Array(twelveHoursFormat.size) { String() })
        } else {
            hourPicker.maxValue = twentyFourHoursFormat.size
            hourPicker.displayedValues = twentyFourHoursFormat.toArray(Array(twentyFourHoursFormat.size) { String() })
        }
        hourPicker.setOnValueChangedListener { _, _, newVal ->
            hour = if (MainActivity.timeFormat == 12) {
                if (formatPicker.value == 2)
                    if (newVal != 12)
                        newVal + 12
                    else newVal
                else
                    if (newVal != 12)
                        newVal
                    else newVal + 12
            } else
                if (newVal == 24) 0
                else newVal
            calendar.set(Calendar.HOUR_OF_DAY, hour)
            resetCalendar()
            calendar.timeInMillis += getTimeToDate() ?: 0
            textTimeLeft.text = calcTimeLeft(calendar.timeInMillis, getTimeToDate() != null)
        }

        val mins = arrayListOf<String>()
        for (num in 1..60) {
            if (num < 10)
                mins.add("0$num")
            else if (num == 60)
                mins.add("00")
            else
                mins.add(num.toString())
        }
        minPicker.minValue = 1
        minPicker.maxValue = mins.size
        minPicker.displayedValues = mins.toArray(Array(mins.size){ String() })
        minPicker.setOnValueChangedListener { _, _, newVal ->
            min = if (newVal == 60) 0
                else newVal
            calendar.set(Calendar.MINUTE, min)
            resetCalendar()
            //Log.d(TAG, "MIN: $calendar.timeInMillis")
            calendar.timeInMillis += getTimeToDate() ?: 0
            textTimeLeft.text = calcTimeLeft(calendar.timeInMillis, getTimeToDate() != null)
        }

        if (MainActivity.timeFormat == 24) {
            val div1 = view.findViewById<ImageView>(R.id.imageView8)
            val div2 = view.findViewById<ImageView>(R.id.imageView9)
            div1.visibility = View.GONE
            div2.visibility = View.GONE
            formatPicker.visibility = View.GONE
        }

        val timeFormat = arrayOf("am", "pm")
        formatPicker.minValue = 1
        formatPicker.maxValue = timeFormat.size
        formatPicker.displayedValues = timeFormat
        formatPicker.setOnValueChangedListener { _, _, newVal ->
            //Log.d(TAG, "FORMAT: $newVal")
            hour = if (newVal == 2)
                hourPicker.value + 12
            else
                hourPicker.value
            resetCalendar()
            calendar.timeInMillis += getTimeToDate() ?: 0
            textTimeLeft.text = calcTimeLeft(calendar.timeInMillis, getTimeToDate() != null)
        }

        val btnDesc            = view.findViewById<Button>(R.id.button_alarm_description)
        val btnShutdownType    = view.findViewById<Button>(R.id.button_shutdown_type)
        val btnDelete       = activity!!.findViewById<ImageView>(R.id.action_view)
        val btnSound           = view.findViewById<Button>(R.id.button_sound)
        val btnSkip            = view.findViewById<Button>(R.id.button_skip)

        shutdownTypeText.text = "Default"
        soundText.text = printSoundTitle("")

        if (currentAlarmId != 0) {
            currentAlarm = mAlarmViewModel.getAlarmById(currentAlarmId)
            //Log.i(TAG, "Alarm enum: ${currentAlarm}")
            if (MainActivity.timeFormat == 12) {
                if (currentAlarm.hour in 12..23) {
                    formatPicker.value = 2
                    hourPicker.value = currentAlarm.hour - 12
                } else {
                    if (currentAlarm.hour == 24)
                        hourPicker.value = 12
                    else hourPicker.value = currentAlarm.hour
                    formatPicker.value = 1
                }
            } else {
                hourPicker.value    = currentAlarm.hour
            }
            minPicker.value     = currentAlarm.min
            hour                = currentAlarm.hour
            min                 = currentAlarm.min

            if(currentAlarm.daysToRepeat.indexOf(1) != -1) day_Sn.isChecked = true
            if(currentAlarm.daysToRepeat.indexOf(2) != -1) day_Mn.isChecked = true
            if(currentAlarm.daysToRepeat.indexOf(3) != -1) day_Tu.isChecked = true
            if(currentAlarm.daysToRepeat.indexOf(4) != -1) day_Wd.isChecked = true
            if(currentAlarm.daysToRepeat.indexOf(5) != -1) day_Th.isChecked = true
            if(currentAlarm.daysToRepeat.indexOf(6) != -1) day_Fr.isChecked = true
            if(currentAlarm.daysToRepeat.indexOf(7) != -1) day_Sa.isChecked = true

            description                 = currentAlarm.description
            descText.text               = getDescription(description)
            isDefault                   = currentAlarm.isShutdownDefault
            isShake                     = currentAlarm.isShutdownShake
            shakeCount                  = currentAlarm.shakeCount
            sound                       = currentAlarm.sound
            volumeBar.progress          = currentAlarm.volume
            vibrationSwitcher.isChecked = currentAlarm.vibration
            skippingDelayTime           = currentAlarm.skippingDelay
            skippingTimes               = currentAlarm.allowedSkippingTimes
            isGame                      = currentAlarm.isShutdownGame
            gameLevel                   = currentAlarm.gameLevel
            gameTime                    = currentAlarm.gameTime
            shutdownTypeText.text       = printShutdownType()
            soundText.text              = printSoundTitle(currentAlarm.sound)
            skipText.text               = getSkipText()
            btnDelete.visibility        = View.VISIBLE
        } else {
            resetNewAlarmVars()
            btnDelete.visibility        = View.INVISIBLE
        }

        resetCalendar()

        textTimeLeft.text = calcTimeLeft(calendar.timeInMillis)

        btnDesc.setOnClickListener{
            val alarmDialog = AlarmDescriptionDialog()
            alarmDialog.show(childFragmentManager, "MyDialog")
        }

        btnShutdownType.setOnClickListener {
            val intent = Intent(activity!!.applicationContext, ShutdownTypeActivity::class.java)
            intent.putExtra("isDefault", isDefault)
            intent.putExtra("isShake", isShake)
            intent.putExtra("shakeCount", shakeCount)
            intent.putExtra("isGame", isGame)
            intent.putExtra("gameLevel", gameLevel)
            intent.putExtra("gameTime", gameTime)
            startActivityForResult(intent, SHUTDOWN_CODE)
        }

        btnSound.setOnClickListener {
            val intent = Intent(activity!!.applicationContext, SoundActivity::class.java)
            intent.putExtra("sound", sound)
            startActivityForResult(intent, SOUND_CODE)
        }

        btnSkip.setOnClickListener{
            val alarmDialog = AlarmSkipDialog(skippingTimes, skippingDelayTime)
            alarmDialog.show(childFragmentManager, "MyDialog")
        }

        if (currentAlarmId != 0) {
            btnDelete.visibility = View.VISIBLE
        }
        btnDelete.setImageDrawable(activity!!.resources.getDrawable(R.drawable.ic_delete_accent))
        btnDelete.setOnClickListener {
            mAlarmViewModel.deleteAlarm(currentAlarm)
            deleteAlarm(activity!!.applicationContext, currentAlarm)
            mActivity.changeFragment(AlarmsRecyclerFragment())
            // TODO close it
            btnDelete.visibility = View.GONE
        }
        val dayButtonArray = arrayListOf<ToggleButton>(day_Mn, day_Tu, day_Wd,
            day_Th, day_Fr, day_Sa, day_Sn)
        for (day in dayButtonArray) {
            day.typeface = ResourcesCompat.getFont(activity!!, R.font.nunito_bold)
            day.setOnCheckedChangeListener { _, _ ->
                resetCalendar()

                calendar.timeInMillis += getTimeToDate() ?: 0
                textTimeLeft.text = calcTimeLeft(calendar.timeInMillis, getTimeToDate() != null)
                //adapter.notifyDataSetChanged()
            }
        }

        val fab = activity!!.findViewById<FloatingActionButton>(R.id.add_alarm)
        fab.visibility = View.VISIBLE
        fab.setImageDrawable(resources.getDrawable(R.drawable.ic_done))
        fab.setOnClickListener {
            resetCalendar()
            val daysList = arrayListOf<Int>()
            if(day_Sn.isChecked) daysList.add(Calendar.SUNDAY)
            if(day_Mn.isChecked) daysList.add(Calendar.MONDAY)
            if(day_Tu.isChecked) daysList.add(Calendar.TUESDAY)
            if(day_Wd.isChecked) daysList.add(Calendar.WEDNESDAY)
            if(day_Th.isChecked) daysList.add(Calendar.THURSDAY)
            if(day_Fr.isChecked) daysList.add(Calendar.FRIDAY)
            if(day_Sa.isChecked) daysList.add(Calendar.SATURDAY)
            var repeat = false
            if (!daysList.isNullOrEmpty()) repeat = true

            var offsetY = view.height - fab.y + fab.height + 160
            //Log.i(TAG, "CALENDAR TIME IN MILLIES: " + calendar.timeInMillis + " CURRENT TIME: " + Date().time)

            calendar.timeInMillis += getTimeToDate() ?: 0
            activity!!.toast(calcTimeLeft(calendar.timeInMillis, getTimeToDate() != null)).setGravity(Gravity.BOTTOM or Gravity.CENTER, 0, offsetY.toInt())

            if (::currentAlarm.isInitialized && currentAlarmId != 0) {

                deleteAlarm(activity!!.applicationContext, currentAlarm)
                //mAlarmViewModel.deleteAlarm(currentAlarm)
                currentAlarm.hour = hour
                currentAlarm.min = min
                currentAlarm.daysToRepeat = daysList
                currentAlarm.isRepeating = repeat
                currentAlarm.description = description
                currentAlarm.enabled = true
                currentAlarm.sound = sound
                currentAlarm.volume = volumeBar.progress
                currentAlarm.vibration = vibrationSwitcher.isChecked
                currentAlarm.isShutdownDefault = isDefault
                currentAlarm.isShutdownShake = isShake
                currentAlarm.shakeCount = shakeCount
                currentAlarm.skippingDelay = skippingDelayTime
                currentAlarm.allowedSkippingTimes = skippingTimes
                currentAlarm.isShutdownGame = isGame
                currentAlarm.gameLevel = gameLevel
                currentAlarm.gameTime = gameTime

                //Log.d(TAG, "send alarm to add new: $currentAlarm")
                val id = addAlarm(activity!!.applicationContext, calendar, currentAlarm)
                currentAlarm.id = id
                mAlarmViewModel.updateAlarm(currentAlarm)

            } else {
                val _alarm = Alarm(
                    0, min, hour, calendar.timeInMillis, true, daysList, repeat,
                    description = description, isShutdownDefault = isDefault,
                    isShutdownShake = isShake, shakeCount = shakeCount, sound = sound,
                    volume = volumeBar.progress, vibration = vibrationSwitcher.isChecked,
                    skippingDelay = skippingDelayTime, allowedSkippingTimes = skippingTimes,
                    isShutdownGame = isGame, gameLevel = gameLevel, gameTime = gameTime
                )
                val id = addAlarm(activity!!.applicationContext, calendar, _alarm)
                _alarm.id = id
                //Log.d(TAG, "creating new alarm: $_alarm")
                mAlarmViewModel.updateAlarm(_alarm)
            }

            mActivity.changeFragment(AlarmsRecyclerFragment())
        }
        //Log.d(TAG, "currentAlarmId: $currentAlarmId")
    }

    private fun getTimeToDate(): Long? {
        val closestDayId = getClosestDayId()
        var diffMilliseconds = calendar.timeInMillis - Date().time
        // Log.d(TAG, "diffMilliseconds: $diffMilliseconds")
        // if no day checked
        if (closestDayId == null ) {
            if (diffMilliseconds >= 0) {
                return null
            } else {
                //Log.d(TAG, "HARA1")
                return 86400000
            }
        }

        // if current day checked
        if (closestDayId == 0) {
            //Log.d(TAG, "HARA2")
            //Log.d(TAG, "HOW!?????? EXCEPTION!111")
            return if (diffMilliseconds < 0) 86400000
            else 0
        }

        var currentDay = calendar.get(Calendar.DAY_OF_WEEK)
        if (closestDayId == currentDay) {
            return if (diffMilliseconds < 0)  7 * 86400000L
            else  0
        }
        //Log.d(TAG, "current day: $currentDay")
        //Log.d(TAG, "closestDayId: $closestDayId")
        var countDays = 0
        for (n in 1..6) {
            if (closestDayId == currentDay) break
            countDays++
            currentDay++
            if (currentDay == 8) currentDay = 1
        }

        return countDays * 86400000L
    }

    private fun getClosestDayId(): Int? {

        val listDays = arrayListOf<Int>()
        if (day_Sn.isChecked) listDays.add(1)
        if (day_Mn.isChecked) listDays.add(2)
        if (day_Tu.isChecked) listDays.add(3)
        if (day_Wd.isChecked) listDays.add(4)
        if (day_Th.isChecked) listDays.add(5)
        if (day_Fr.isChecked) listDays.add(6)
        if (day_Sa.isChecked) listDays.add(7)

        if (listDays.isEmpty()) return null

        var currentDay = calendar.get(Calendar.DAY_OF_WEEK)
        Log.d(TAG, "curret day: $currentDay")
        var diffMilliseconds = calendar.timeInMillis - Date().time
        val thisWeek = listDays.filter { it > currentDay }.min()
        val nextWeek = listDays.filter { it < currentDay }.min()
        val isCurrentDay = listDays.findLast { it == currentDay }
        var dayId =
            if (thisWeek != null) {
                thisWeek
            } else if (nextWeek != null) { nextWeek }
            else 0

        // this is for current day, if it selected
        if (dayId == thisWeek && isCurrentDay != null && diffMilliseconds > 0) {
            dayId = isCurrentDay
        } else if (thisWeek == null && nextWeek == null && isCurrentDay != null) {
            dayId = isCurrentDay
        }

        return dayId
    }

    private fun resetCalendar() {
        calendar = Calendar.getInstance()
        calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR))
        calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH))
        calendar.set(Calendar.DAY_OF_WEEK, calendar.get(Calendar.DAY_OF_WEEK))
        calendar.set(Calendar.HOUR_OF_DAY, hour)
        calendar.set(Calendar.MINUTE, min)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)
    }

    private fun resetNewAlarmVars() {
        //Log.i(TAG, "RESETTING")
        val cal = Calendar.getInstance()

        hour = cal.get(Calendar.HOUR_OF_DAY)
        min = cal.get(Calendar.MINUTE)
        if (MainActivity.timeFormat == 12) {
            if (hour > 12) {
                formatPicker.value = 2
                hourPicker.value = hour - 12
            } else {
                formatPicker.value = 1
                hourPicker.value = hour
            }
        } else {
            hourPicker.value = hour
        }
        minPicker.value = min

        day_Sn.isChecked = false
        day_Mn.isChecked = false
        day_Tu.isChecked = false
        day_Wd.isChecked = false
        day_Th.isChecked = false
        day_Fr.isChecked = false
        day_Sa.isChecked = false

        description                 = null
        descText.text               = getDescription(null)
        isDefault                   = true
        isShake                     = false
        shakeCount                  = 5
        sound                       = null
        volumeBar.progress          = 100
        vibrationSwitcher.isChecked = true
        shutdownTypeText.text       = printShutdownType()
        soundText.text              = printSoundTitle(null)
        skipText.text               = getSkipText()
        skippingDelayTime           = 60000
        skippingTimes               = 0
        isGame                      = false
        gameLevel                   = 1
        gameTime                    = 5000
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            SHUTDOWN_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    val isDefault = data!!.getBooleanExtra("isDefault", true)
                    val isShake = data.getBooleanExtra("isShake", false)
                    val shakeCount = data.getIntExtra("shakeCount", 0)
                    val isGame = data.getBooleanExtra("isGame", false)
                    val gameLevel = data.getIntExtra("gameLevel", 1)
                    val gameTime = data.getLongExtra("gameTime", 5000)
                    if (::currentAlarm.isInitialized) {
                        currentAlarm.isShutdownDefault = isDefault
                        currentAlarm.isShutdownShake = isShake
                        currentAlarm.shakeCount = shakeCount
                        currentAlarm.isShutdownGame = isGame
                        currentAlarm.gameLevel = gameLevel
                        currentAlarm.gameTime = gameTime
                        mAlarmViewModel.updateAlarm(currentAlarm)
                    }
                    this.isDefault = isDefault
                    this.isShake = isShake
                    this.shakeCount = shakeCount
                    this.isGame = isGame
                    this.gameLevel = gameLevel
                    this.gameTime = gameTime
                    shutdownTypeText.text = printShutdownType()
                }
            }
            SOUND_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    val sound = data!!.getStringExtra("soundUri")
                    Log.d(TAG, "Get sound uri: $sound")
                    if (::currentAlarm.isInitialized) {
                        currentAlarm.sound = sound
                        mAlarmViewModel.updateAlarm(currentAlarm)
                    }
                    Log.d(TAG, sound)
                    this.sound = sound
                }
                soundText.text = printSoundTitle(sound)
            }
        }
        Log.i(TAG, "OnActivityResult()")
        //animateBottomBar = true
    }

    private fun getDescription(string: String?) =
        if (string.isNullOrEmpty()) "No title" else string

    private fun printShutdownType(): String {
        val listTypes = arrayListOf<String>()
        if (isDefault) listTypes.add("Default")
        if (isShake && shakeCount > 0) listTypes.add("Shakes: $shakeCount")
        if (isGame && gameLevel > 0 && gameTime >= 5000) listTypes.add("Game")
        return listTypes.joinToString()
    }

    private fun printSoundTitle(soundUri: String?): String {

        if (soundUri.isNullOrBlank()) return "Default"
        if (!getAppSoundNameByUri(soundUri).isNullOrBlank()) return getAppSoundNameByUri(soundUri)!!
        val cursor = activity!!.contentResolver.query(Uri.parse(soundUri), null, null, null, null)
        val titleIndex = cursor.getColumnIndex(MediaStore.Audio.Media.TITLE)
        cursor!!.moveToFirst()
        //val audioTitle: String = cursor.getString(titleIndex)
        //cursor!!.moveToFirst()
        return cursor.getString(titleIndex)
    }

    private fun getSkipText(): String =
        if (skippingTimes == 0)
            "No skipping"
        else {
            val str = if (skippingTimes > 1) "times" else "time"
            "$skippingTimes $str with ${skippingDelayTime.div(60000)} min delay"
        }

    override fun sendInput(input: String?) {
        description = input
        descText.text = getDescription(description)
    }

    override fun sendSkipResult(input: ArrayList<Int>) {
        skippingTimes = input[0]
        skippingDelayTime = input[1].toLong()
        skipText.text = getSkipText()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("min", currentAlarmId)
        if (currentAlarmId != 0)
            outState.putInt("currentAlarmId", currentAlarmId)
    }
}