package com.michael.alarmclock.alarm

import android.app.PendingIntent
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.michael.alarmclock.R
import com.michael.alarmclock.alarm.service.NotificationService
import com.michael.alarmclock.alarm.util.setAlarm
import com.michael.alarmclock.data.alarm.Alarm
import java.util.*

class GameStopActivity: AppCompatActivity() {

    private val alarm by lazy { intent.getParcelableExtra<Alarm>("alarm") }
    private val TAG = "GameStopActivity"

    companion object {
        var level = 3L
        var isRetry = false
        var isWon = false
        var isDisabled = false
        var isSkipped = false
        var currentAlarm = Alarm()
    }

    private var isFragmentShowed = false

    fun changeFragment(changeTo: String) {
        val fragment =
            if (changeTo == "pre_game")
                PreGameFragment()
            else
                GameFragment()
        replaceFragment(fragment, R.id.frame_layout)
    }

    private inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> Unit) {
        val fragmentTransaction = beginTransaction()
        fragmentTransaction.func()
        fragmentTransaction.commit()
    }

    private fun AppCompatActivity.addFragment(fragment: Fragment, frameId: Int){
        supportFragmentManager.inTransaction { add(frameId, fragment) }
    }

    private fun AppCompatActivity.replaceFragment(fragment: Fragment, frameId: Int) {
        supportFragmentManager.inTransaction{replace(frameId, fragment)}
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)

        isFragmentShowed = savedInstanceState?.getBoolean("isFragmentShowed", false) ?: false
        isWon = savedInstanceState?.getBoolean("isWon", false) ?: false

        currentAlarm = alarm
        level = currentAlarm.gameLevel.toLong()
        Log.d(TAG, "ALARM: $currentAlarm")
        val fragment =
            if (isWon)
                GameFragment()
            else
                PreGameFragment()

        if (!isFragmentShowed) {
            isFragmentShowed = true
            addFragment(fragment, R.id.frame_layout)
        } else {
            replaceFragment(fragment, R.id.frame_layout)
        }

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putBoolean("isFragmentShowed", isFragmentShowed)
        outState.putBoolean("isWon", isWon)
    }

    fun finishActivity() {
        this@GameStopActivity.finishAndRemoveTask()
    }

//    override fun onDestroy() {
//        Log.d("TAG", "onDestroy()")
//        super.onDestroy()
//        System.exit(0)
//    }

    override fun onDestroy() {
        //Log.i(TAG, "onDestroy")
        if (!isSkipped) {
            val serviceIntent = Intent(this, NotificationService::class.java)
            serviceIntent.putExtra("isGameStopped", isDisabled)
            serviceIntent.putExtra("alarm", alarm)
            ContextCompat.startForegroundService(this, serviceIntent)
        } else {
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = Date().time + alarm.skippingDelay
            Log.d(TAG, "skip for: " + alarm.skippingDelay.div(60000) + " mins")

            val myIntent = Intent(this.applicationContext, AlarmReceiver::class.java)
            //myIntent.putExtra("isSkipping", true)
            //Log.d(TAG, "alarm: $alarm")
            myIntent.putExtra("alarmId", alarm.id)
            myIntent.putExtra("skipLeft", alarm.allowedSkippingTimes)
            myIntent.action = App.SKIPPED
            val pendingIntent =
                PendingIntent.getBroadcast(this.applicationContext, alarm.id, myIntent, PendingIntent.FLAG_UPDATE_CURRENT)
            setAlarm(this.applicationContext, calendar.timeInMillis, pendingIntent)

            val serviceIntent = Intent(this, NotificationService::class.java)
            serviceIntent.putExtra("isSkipped", isSkipped)
            serviceIntent.putExtra("alarm", alarm)
            ContextCompat.startForegroundService(this, serviceIntent)
        }
        super.onDestroy()
    }
}