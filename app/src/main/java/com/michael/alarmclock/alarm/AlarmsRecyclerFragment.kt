package com.michael.alarmclock.alarm


import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import android.widget.*
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.michael.alarmclock.R
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.michael.alarmclock.data.alarm.Alarm
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.Observer
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.ItemTouchHelper
import com.michael.alarmclock.MainActivity
import com.michael.alarmclock.alarm.AlarmsRecyclerAdapter.Companion.updatingRecyclerViewDelay
import com.michael.alarmclock.alarm.util.deleteAlarm
import com.michael.alarmclock.settings.SettingsFragment


class AlarmsRecyclerFragment : Fragment(), MainActivity.IOnBackPressed {

    private val TAG = "AlarmsRecyclerFragment"
    private val mAlarmViewModel by lazy { ViewModelProviders.of(this)[AlarmViewModel::class.java] }
    private val mFragment by lazy { this }
    private var alarmsList = listOf<Alarm>()
    private lateinit var fab: FloatingActionButton
    private val mActivity by lazy { activity as MainActivity}


    companion object {
        var cardWidth = 0
    }

    override fun onBackPressed() {
        Log.d(TAG, "BACK!1 isBackPressed: $MainActivity.isBackPressed")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_alarms_recycler, container, false)
    }

    override fun onResume() {
        super.onResume()
        Log.i(TAG, "onResume()")
    }

    val adapter by lazy { AlarmsRecyclerAdapter(activity!!.applicationContext, mFragment) }

    @SuppressLint("RestrictedApi")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.i(TAG, "onViewCreated()")

        val btnAction    = activity!!.findViewById<ImageView>(R.id.action_view)
        btnAction.setImageDrawable(activity!!.resources.getDrawable(R.drawable.ic_settings_white))
        btnAction.visibility = View.VISIBLE
        btnAction.setOnClickListener {
            mActivity.changeFragment(SettingsFragment())
        }

        val txtToolbar    = activity!!.findViewById<TextView>(R.id.text_toolbar)
        txtToolbar.text = "Alarms"


        cardWidth = getScreenHeightInDPs(activity!!.applicationContext) - resources.getDimension(R.dimen.alarm_item_recycle_side_margin).toInt()
        val recyclerView = view!!.findViewById<RecyclerView>(R.id.alarms_list)
        //val adapter = AlarmsRecyclerAdapter(activity!!.applicationContext, mFragment)
        //isOpenedAlarmEdit = savedInstanceState?.getBoolean("isOpenedAlarmEdit", false) ?: false

        fab = activity!!.findViewById(R.id.add_alarm)
        fab.setImageDrawable(resources.getDrawable(R.drawable.ic_add))
        fab.visibility = View.VISIBLE
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(activity!!.applicationContext)
        adapter.onItemClick = { alarm, view ->

            // do something with your item
            //Log.d("TAG", "view height: " + view.y)
            mActivity.changeFragment(AlarmEditFragment(alarm.id))
            //val intent = Intent(activity, AlarmActivity::class.java)
            //startActivity(intent)
            //showBar(false, false, true)
        }

        // Add an observer on the LiveData returned by getAlphabetizedWords.
        // The onChanged() method fires when the observed data changes and the activity is
        // in the foreground.
        //editLayout.visibility = View.GONE

        val emptyView = view.findViewById<TextView>(R.id.empty_view)
        mAlarmViewModel.getAlarmsLiveData().observe(this,
            Observer<List<Alarm>> { alarms ->
                // Update the cached copy of the words in the adapter.
                if (alarms.isNullOrEmpty()) {
                    emptyView.visibility = View.VISIBLE
                } else {
                    emptyView.visibility = View.GONE
                }
                val sortedList = alarms?.sortedWith(compareBy({ it.hour }, { it.min }))
                adapter.setAlarms(sortedList)
                //Handler().postDelayed({
                adapter.notifyDataSetChanged()
                updatingRecyclerViewDelay = 0L
                //}, updatingRecyclerViewDelay)
                // for sort
                if (!sortedList.isNullOrEmpty()) alarmsList = sortedList
            })
//        recyclerView.addItemDecoration(DividerItemDecoration(activity!!.applicationContext,
//            DividerItemDecoration.VERTICAL))
        recyclerView.layoutManager = LinearLayoutManager(activity!!.applicationContext)
        val swipeHandler = object : SwipeToDeleteCallback(activity!!.applicationContext) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                // TODO soft animation: https://medium.com/@zackcosborn/step-by-step-recyclerview-swipe-to-delete-and-undo-7bbae1fce27e
                val alarm = mAlarmViewModel.getAlarmsLiveData().value!![viewHolder.adapterPosition]
                val position = viewHolder.adapterPosition
                adapter.deleteItem(position)
                Log.d(TAG, "onSwiped: Alarm: $alarm")
                updatingRecyclerViewDelay = 500
                deleteAlarm(activity!!.applicationContext, alarmsList!![position])
                mAlarmViewModel.deleteAlarm(alarmsList!![position])
                //showBar(true)
            }

            override fun getSwipeDirs(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int {
                return if (viewHolder.itemViewType == 1 ) 0 else super.getSwipeDirs(recyclerView, viewHolder)
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeHandler)
        itemTouchHelper.attachToRecyclerView(recyclerView)

        fab.setOnClickListener {
            mActivity.changeFragment(AlarmEditFragment())
        }

    }

    private fun getScreenHeightInDPs(context: Context): Int {
        /*
            DisplayMetrics
                A structure describing general information about a display,
                such as its size, density, and font scaling.
        */
        val dm = DisplayMetrics()

        val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        windowManager.defaultDisplay.getMetrics(dm)
        return Math.round(dm.widthPixels / dm.density)
    }

//    private fun openViewAlarmAdd(v: View) {
//
//        // TODO каряво открывается по высоте при нажатии на будильник
//        val editLayout = view!!.findViewById<ScrollView>(R.id.alarm_edit_layout)
//        val x = v.x.toInt() + v.width / 2
//        val y = v.y.toInt() - v.height
//
//        if (!isOpenedAlarmEdit) {
//
//            val startRadius = 0f
//            val endRadius = Math.hypot(view!!.width.toDouble(), view!!.height.toDouble()).toFloat()
//
//            val anim = ViewAnimationUtils.createCircularReveal(editLayout, x, y, startRadius, endRadius)
//
//            editLayout.visibility = View.VISIBLE
//            anim.start()
//
//
//            fab.setImageDrawable(resources.getDrawable(R.drawable.ic_done))
//            isOpenedAlarmEdit = true
//
//        } else {
//            val startRadius = Math.hypot(view!!.width.toDouble(), view!!.height.toDouble()).toFloat()
//            val endRadius = 0f
//
//            val anim = ViewAnimationUtils.createCircularReveal(editLayout, x, y, startRadius, endRadius)
//            anim.addListener(object : Animator.AnimatorListener {
//                override fun onAnimationStart(animation: Animator?) { }
//
//                override fun onAnimationEnd(animation: Animator?) {
//                    Log.d(TAG, "onAnimationEnd()")
//                    editLayout.visibility = View.GONE
//                }
//
//                override fun onAnimationCancel(animation: Animator?) { }
//                override fun onAnimationRepeat(animation: Animator?) { }
//            })
//            fab.setImageDrawable(resources.getDrawable(R.drawable.ic_add))
//            anim.start()
//            isOpenedAlarmEdit = false
//        }
//    }

    override fun onDestroy() {
        Log.d(TAG, "onDestroy()")
        super.onDestroy()
    }
}