package com.michael.alarmclock.alarm

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import com.michael.alarmclock.R
import java.lang.ClassCastException
import android.view.inputmethod.InputMethodManager
import android.widget.ImageButton
import androidx.core.content.ContextCompat.getSystemService



class AlarmDescriptionDialog : DialogFragment() {

    var description: String? = ""
    lateinit var inputDesc: EditText
    lateinit var btnOK: ImageButton
    lateinit var btnBack: ImageButton

    private val TAG = "AlarmDescriptionDialog"

    interface OnInputListener {
        fun sendInput(input: String?)
    }

    var mOnInputListener: OnInputListener? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var myView = inflater.inflate(R.layout.dialog_alarm_description, container, false)

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        setStyle(STYLE_NO_FRAME, 0)
        super.onCreateView(inflater, container, savedInstanceState)

        inputDesc = myView.findViewById(R.id.dialog_alarm_desc_input_description)
        btnOK = myView.findViewById(R.id.dialog_alarm_desc_OK)
        btnBack = myView.findViewById(R.id.dialog_alarm_desc_back)

        btnOK.setOnClickListener{
            description = inputDesc.text.toString()

            mOnInputListener!!.sendInput(description)
            dismiss()
        }

        btnBack.setOnClickListener{
            dismiss()
        }

        inputDesc.requestFocus()
        dialog.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)

        return myView
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try {
            Log.i(TAG, "onAttach() $parentFragment")
            mOnInputListener = parentFragment as OnInputListener?
        } catch (e: ClassCastException) {
            Log.e(TAG, "onAttach: ClassCastException: " + e.message)
        }

    }
}