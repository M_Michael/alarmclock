package com.michael.alarmclock.alarm

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import androidx.appcompat.app.AppCompatActivity
import android.widget.Button
import com.michael.alarmclock.R
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import com.michael.alarmclock.data.alarm.Alarm
import java.util.*
import androidx.core.content.ContextCompat
import com.michael.alarmclock.alarm.service.NotificationService
import com.michael.alarmclock.alarm.util.addAlarm
import com.michael.alarmclock.alarm.util.setAlarm


class DefaultStopActivity : AppCompatActivity() {

    private val alarm by lazy { intent.getParcelableExtra<Alarm>("alarm") }
    private var isDisabled = false
    private var isSkipped = false
    private val TAG = "DefaultStopActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.stop_activity_default_alarm)
        //Log.i(TAG, "onCreate")
        //Log.d(TAG, "skip counts: ${alarm.allowedSkippingTimes}")

        window.addFlags(
            WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON or
            WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD or
            WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED or
            WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
        )

        val btnSkip = findViewById<Button>(R.id.button_skip_default)
        val btnStop = findViewById<Button>(R.id.button_stop_alarm)
        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val descText = findViewById<TextView>(R.id.stop_alarm_description)
        descText.text = alarm.description

        btnSkip.text = "Skip alarm for ${alarm.skippingDelay / 60000} min"
        if (alarm.allowedSkippingTimes > 0) {
            btnSkip.visibility = View.VISIBLE
        } else {
            btnSkip.visibility = View.GONE
        }

        btnSkip.setOnClickListener {
            isSkipped = true
            alarm.allowedSkippingTimes --
            this@DefaultStopActivity.finish()
        }

        var myIntent = Intent(this, AlarmReceiver::class.java)
        btnStop.setOnClickListener{
            if (alarm.daysToRepeat.isNotEmpty() && alarm.isRepeating) {
                val pendingIntent = PendingIntent.getBroadcast(
                    this@DefaultStopActivity,
                    alarm.id,
                    myIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT
                )
                alarmManager.cancel(pendingIntent)
            }

            App.setOffOutOfDateAlarms(this)
            isDisabled = true

            this@DefaultStopActivity.finishAndRemoveTask()
        }
    }

    override fun onDestroy() {
        Log.i(TAG, "onDestroy")
        if (!isSkipped) {
            val serviceIntent = Intent(this, NotificationService::class.java)
            serviceIntent.putExtra("isDefaultStopped", isDisabled)
            serviceIntent.putExtra("alarm", alarm)
            ContextCompat.startForegroundService(this, serviceIntent)
        } else {
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = Date().time + alarm.skippingDelay
            Log.d(TAG, "skip for: " + alarm.skippingDelay.div(60000) + " mins")

            val myIntent = Intent(this.applicationContext, AlarmReceiver::class.java)
            //myIntent.putExtra("isSkipping", true)
            //Log.d(TAG, "alarm: $alarm")
            myIntent.putExtra("alarmId", alarm.id)
            myIntent.putExtra("skipLeft", alarm.allowedSkippingTimes)
            myIntent.action = App.SKIPPED
            val pendingIntent =
                PendingIntent.getBroadcast(this.applicationContext, alarm.id, myIntent, PendingIntent.FLAG_UPDATE_CURRENT)
            setAlarm(this.applicationContext, calendar.timeInMillis, pendingIntent)

            val serviceIntent = Intent(this, NotificationService::class.java)
            serviceIntent.putExtra("isSkipped", isSkipped)
            serviceIntent.putExtra("alarm", alarm)
            ContextCompat.startForegroundService(this, serviceIntent)
        }
        super.onDestroy()
    }
}