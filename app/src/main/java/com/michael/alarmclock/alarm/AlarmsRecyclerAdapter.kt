package com.michael.alarmclock.alarm

import android.content.Context
import android.graphics.drawable.Animatable
import android.os.Build
import android.util.Log
import android.view.*
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.ads.*
import com.michael.alarmclock.R
import com.michael.alarmclock.alarm.util.*
import com.michael.alarmclock.data.alarm.Alarm
import kotlinx.android.synthetic.main.ad_recycler_item.view.*
import kotlinx.android.synthetic.main.new_alarm_item.view.*
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.res.ResourcesCompat
import com.michael.alarmclock.MainActivity


class AlarmsRecyclerAdapter(context: Context, fragment: AlarmsRecyclerFragment) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var onItemClick: ((Alarm, View) -> Unit)? = null
    //private val AD_UNIT_POSITION = 5 //After Every N-1 i.e. every 5th item would be an ad (After 5-1)
    private val VIEW_TYPE_NORMAL = 0
    private val VIEW_TYPE_ADMOB = 1
    private var mAlarms: MutableList<Alarm>? = null
    private val mAlarmViewModel by lazy { ViewModelProviders.of(fragment)[AlarmViewModel::class.java] }
    private val context = context.applicationContext
    private var mInflater = LayoutInflater.from(context)
    val fragment = fragment
    private val TAG = "AlarmsRecyclerAdapter"

    companion object {
        var updatingRecyclerViewDelay = 0L
    }

    inner class AlarmItemHolder(v: View, a: Alarm = Alarm()) : RecyclerView.ViewHolder(v) {
        val view: View = v
        val alarm: Alarm = a

        init {
            //v.setOnClickListener(this)
            v.setOnClickListener {
                onItemClick?.invoke(alarm, v)
            }
        }
//        override fun onClick(v: View) {
//            val intent = Intent(v.context, AlarmActivity::class.java)
//            intent.putExtra("alarm_id", alarm.id)
//            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
//            v.context.startActivity(intent)
//            fragment.activity!!.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
//        }
    }

    inner class AdmobViewHolder internal constructor(view: View) : RecyclerView.ViewHolder(view) {
        val view = view
    }

    override fun getItemViewType(position: Int): Int {
        return if (mAlarms?.size!!.compareTo(position) <= 0)
            VIEW_TYPE_ADMOB
        else
            VIEW_TYPE_NORMAL
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val viewType = getItemViewType(position)
        when (viewType) {
            VIEW_TYPE_NORMAL -> {
                placeNormalContent(holder, position)
            }
            VIEW_TYPE_ADMOB -> {
                placeAdMobAd(holder, position)
                //refreshAd(holder, position)
            }
            else -> {
                placeAdMobAd(holder, position)
                //refreshAd(holder, position)
            }
        }
    }

    private fun placeAdMobAd(holder: RecyclerView.ViewHolder, position: Int) {
        val bannerHolder = holder as AdmobViewHolder
        //val adRequest = PublisherAdRequest.Builder().build()
        val adContainer = bannerHolder.view.adMobView
        //val customAdSize = AdSize(holder.view.alarm_card.width, holder.view.alarm_card.height)
        Log.d(TAG, "width: ${AlarmsRecyclerFragment.cardWidth}")
        val customAdSize = AdSize(AlarmsRecyclerFragment.cardWidth, 178)
        val mAdView = AdView(context)
        mAdView.adSize = customAdSize
        mAdView.adUnitId = "ca-app-pub-6317774061864467/9639001622" //ca-app-pub-3940256099942544/6300978111 /6499/example/banner ca-app-pub-3940256099942544/2247696110
        (adContainer as RelativeLayout).addView(mAdView)
        val adRequest = AdRequest.Builder().addTestDevice("CE5BB33C69A3CD003F7FFC052DEBE090").build()
        mAdView.loadAd(adRequest)
        //mPublisherAdView.loadAd(adRequest)
    }

    private fun placeNormalContent(holder: RecyclerView.ViewHolder, position: Int) {

        if (mAlarms == null) return

        val holder = holder as AlarmItemHolder
        val current = mAlarms!![position]
        //holder.view.alarm_id.text = current.id.toString()
        val typeface = ResourcesCompat.getFont(context, R.font.nunito_bold)
        holder.view.alarm_item_time.text = current.printTime()
        if (MainActivity.timeFormat == 12) {
            holder.view.alarm_item_time_format.visibility = View.VISIBLE
            holder.view.alarm_item_time_format.typeface = typeface
            holder.view.alarm_item_time_format.text =
                if (current.hour in 12..23) "pm"
                else "am"
        } else
            holder.view.alarm_item_time_format.visibility = View.INVISIBLE
        holder.view.alarm_item_time.typeface = typeface
        holder.view.alarm_item_description.text = current.printDescription()
        holder.view.alarm_item_description.typeface = typeface
        val mountainImage = when {
            current.hour >= 12 -> context.getDrawable(R.drawable.ic_mountain_pm)
            else -> context.getDrawable(R.drawable.ic_mountain_am)
        }
        holder.view.mountain_image_view.setImageDrawable(mountainImage)
        holder.alarm.id = current.id
        //Log.d(TAG, "HERE, pos=$position")
        holder.view.alarm_status_switcher.isChecked = current.enabled

        val layout = holder.view.days_ww as ConstraintLayout
        layout.removeViews(0, layout.childCount)
        if (current.isRepeating && !current.daysToRepeat.isNullOrEmpty()) {
            // TODO somehow clear text views here. Maybe add ids to rhem an clear by ids
            val textViewsList = mutableListOf<TextView>()
            for (day in current.daysToRepeat) {
                val view = when (day) {
                    1 -> createTextViewithText("Su")
                    2 -> createTextViewithText("Mo")
                    3 -> createTextViewithText("Tu")
                    4 -> createTextViewithText("We")
                    5 -> createTextViewithText("Th")
                    6 -> createTextViewithText("Fr")
                    7 -> createTextViewithText("Sa")
                    else -> null
                }
                if (view != null) {
                    layout.addView(view)
                    textViewsList.add(view)
                }
            }
            //Log.d(TAG, "COUNT: " + textViewsList.count())
            val set = ConstraintSet()
            set.clone(layout)
            var last: TextView? = null
            for (view in textViewsList) {
                if (last != null) {
                    set.connect(view.id, ConstraintSet.LEFT, last.id, ConstraintSet.RIGHT, 11)
                    set.connect(view.id, ConstraintSet.BOTTOM, layout.id, ConstraintSet.BOTTOM, 0)
                } else {
                    set.connect(view.id, ConstraintSet.LEFT, layout.id, ConstraintSet.LEFT, 11)
                    set.connect(view.id, ConstraintSet.BOTTOM, layout.id, ConstraintSet.BOTTOM, 0)
                }
                set.constrainWidth(view.id, WRAP_CONTENT)
                set.constrainHeight(view.id, WRAP_CONTENT)
                last = view
            }


            set.applyTo(layout)
            //holder.view.text_repeat.text = "Repeats: ${current.printRepeatDays()}"
        } else {
            //holder.view.text_repeat.text = "No repeating."
        }

        holder.view.alarm_status_switcher.setOnClickListener {
            //Log.i("Enabled: ", current.enabled.toString())
            if (current.enabled) {
                deleteAlarm(context, current)
                holder.view.alarm_status_switcher.buttonDrawable = context.getDrawable(R.drawable.anim_alarm_off)
            } else {
                holder.view.alarm_status_switcher.buttonDrawable = context.getDrawable(R.drawable.anim_alarm_on)
            }
            if (Build.VERSION.SDK_INT >= 23) {
                val anim = holder.view.alarm_status_switcher.buttonDrawable as Animatable
                anim.start()
            }
            current.enabled = !current.enabled
            updatingRecyclerViewDelay = 200
            //Log.i("Enabled: ", current.enabled.toString())
            mAlarmViewModel.insert(current)
        }
    }

    private fun createTextViewithText(text: String): TextView {
        val textView = TextView(context)
        textView.id = View.generateViewId()
        textView.text = text
        textView.textSize = 14F
        textView.typeface = ResourcesCompat.getFont(context, R.font.nunito_bold)
        textView.gravity = Gravity.CENTER_HORIZONTAL or Gravity.CENTER_VERTICAL
        textView.setTextColor(context.resources.getColor(R.color.primaryTextColor))
        textView.background = context.resources.getDrawable(R.drawable.day_background_circle)
        return textView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return when (viewType) {
            VIEW_TYPE_NORMAL -> {
                val inflatedView = mInflater.inflate(R.layout.new_alarm_item, parent, false)
                AlarmItemHolder(inflatedView)
            }
            VIEW_TYPE_ADMOB -> {
                val bannerLayoutView = mInflater.inflate(R.layout.ad_recycler_item, parent, false)
                AdmobViewHolder(bannerLayoutView)
            }
            else -> {
                val bannerLayoutView = mInflater.inflate(R.layout.ad_recycler_item, parent, false)
                AdmobViewHolder(bannerLayoutView)
            }
        }
    }

    override fun getItemCount(): Int {
        var count = mAlarms?.count() ?: 0
        return if (count > 0) count + 1
            else 0
    }

    fun setAlarms(alarms: List<Alarm>?) {
        //Log.d(TAG, "setAlarms()")
        //Timer("SettingUp", false).schedule(updatingRecyclerViewDelay) {

            mAlarms = alarms!!.toMutableList()

        //}

    }

    fun deleteItem(position: Int) {
        if (itemCount == 2) notifyItemRemoved(0)
        mAlarms!!.removeAt(position)
        notifyItemRemoved(position)
    }
}


