package com.michael.alarmclock.alarm

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
//import com.michael.alarmclock.data.alarm.ShutdownType

class Converters {
    @TypeConverter
    fun fromString(value: String): ArrayList<Int> {
        val listType = object : TypeToken<ArrayList<Int>>() {

        }.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromArrayList(list: List<Int>): String {
        val gson = Gson()
        //Log.i("Gson", gson.toJson(list))
        return gson.toJson(list)
    }

//    @TypeConverter
//    fun restoreEnum(shutdownType: String): ShutdownType = ShutdownType.valueOf(shutdownType)
//
//    @TypeConverter
//    fun saveEnumToString(shutdownType: ShutdownType) = shutdownType.name
}