package com.michael.alarmclock.alarm.service

import android.Manifest
import android.app.*
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.util.Log
import androidx.core.app.NotificationCompat
import com.michael.alarmclock.R
import android.media.AudioManager
import android.media.MediaPlayer
import com.michael.alarmclock.data.alarm.Alarm
import android.content.BroadcastReceiver
import android.content.IntentFilter
import android.content.ComponentName
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Criteria
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.*
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.PermissionChecker
import com.google.gson.Gson
import com.michael.alarmclock.alarm.*
import kotlin.properties.Delegates




class NotificationService : Service() {
    private val TAG = "NotificationService"

    private lateinit var alarm: Alarm

    private val mainHandler = Handler(Looper.getMainLooper())
    private val vibrator by lazy { getSystemService(Context.VIBRATOR_SERVICE) as Vibrator }
    private val mMediaPlayer = MediaPlayer()
    private var isShutdownDefault = true
    private var isShutdownShake = false
    private var shakeCount = 0
    private var isShutdownGame = false
    private var gameTime = 5000L
    private var gameLevel = 1
    private var steps = 0
    private var currentStep = 1
    private var constDeviceVolume = 0
    private val context = this

    override fun onCreate() {
        super.onCreate()
        Log.i(TAG, "onCreate")

        mainHandler.post(checkCurrentTopActivityTask)
    }

    override fun onBind(intent: Intent?): IBinder? {
        Log.i(TAG, "onBind")
        return null
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        Log.i(TAG, "onStartCommand")

        if (intent.getBooleanExtra("isSkipped", false)) {
            //Log.d(TAG, "HERE")
            stopAlarm()
            stopSelf()
        }

        val sharedPreferences = getSharedPreferences(
            App.SHARED_PREFS,
            Context.MODE_PRIVATE
        )
        val disableVolumeChange = sharedPreferences.getBoolean(App.DISABLE_VOLUME, false)
        if (!disableVolumeChange) {
            val filter = IntentFilter()
            filter.addAction("android.media.VOLUME_CHANGED_ACTION")
            registerReceiver(broadcastReceiver, filter)
        }

        alarm = intent.getParcelableExtra("alarm") as Alarm
        if (intent.getBooleanExtra("isShakeStopped", false))
            isShutdownShake = false
        if (intent.getBooleanExtra("isDefaultStopped", false))
            isShutdownDefault = false
        if (intent.getBooleanExtra("isGameStopped", false))
            isShutdownGame = false

        if (intent.getBooleanExtra("start", false)) {
            if (alarm.isShutdownDefault)    steps ++
            if (alarm.isShutdownShake)      steps ++
            if (alarm.isShutdownGame)       steps ++
            isShutdownDefault   = alarm.isShutdownDefault
            isShutdownShake     = alarm.isShutdownShake
            shakeCount          = alarm.shakeCount
            isShutdownGame      = alarm.isShutdownGame
            gameLevel           = alarm.gameLevel
            gameTime            = alarm.gameTime
        }
        if (steps > 1) {
            currentStep = 1
            if (alarm.isShutdownDefault && !isShutdownDefault)  currentStep ++
            if (alarm.isShutdownShake && !isShutdownShake)      currentStep ++
            if (alarm.isShutdownGame && !isShutdownGame)        currentStep ++
        }

        val notificationIntent = when {
            isShutdownGame -> Intent(this, GameStopActivity::class.java)
            isShutdownShake -> Intent(this, ShakeStopActivity::class.java)
            else -> Intent(this, DefaultStopActivity::class.java)
        }

        notificationIntent.putExtra("alarm", alarm)
        val pendingIntent = PendingIntent.getActivity(
            this,
            0, notificationIntent, 0
        )
        var contentText = "Stop alarm"
        if (steps > 1) contentText += ": step $currentStep of $steps"
        val channelId =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                createNotificationChannel("my_service", App.CHANNEL_ID)
            } else {
                // If earlier version channel ID is not used
                // https://developer.android.com/reference/android/support/v4/app/NotificationCompat.Builder.html#NotificationCompat.Builder(android.content.Context)
                ""
            }

        val notificationBuilder = NotificationCompat.Builder(this, channelId )
        val notification = notificationBuilder.setOngoing(true)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle("Alarm ringing...")
            .setContentText(contentText)
            .setContentIntent(pendingIntent)
            .setPriority(Notification.PRIORITY_HIGH)
            .setCategory(Notification.CATEGORY_SERVICE)
            .build()
        startForeground(101, notification)
        //Log.d(TAG, "START ALARM????????????????: $isShutdownDefault")

        // TODO resolve exception
        try {
            if (!mMediaPlayer.isPlaying)
                playAlarm()
        } catch (e: Exception) {
            Log.e(TAG, "error: " + e.message)
            stopSelf()
        }
        setVibration()
        if (!isShutdownGame && !isShutdownShake && !isShutdownDefault) {
            //Log.d(TAG, "STOPPING SERVICE...")
            stopAlarm()
            val switchOnOff = sharedPreferences.getBoolean(App.SWITCH_WEATHER, false)

            if (switchOnOff && checkLocationPermission()) {
                showWeatherActivity()
            }
            stopSelf()
        }

        return START_NOT_STICKY
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(channelId: String, channelName: String): String{
        val chan = NotificationChannel(channelId,
            channelName, NotificationManager.IMPORTANCE_NONE)
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return channelId
    }

    private fun stopAlarm() {
        mMediaPlayer.stop()
        vibrator.cancel()
        mMediaPlayer.release()
    }

    private fun setVibration() {
        if (alarm.vibration) {
            vibrator.cancel()
            val pattern = longArrayOf(500, 500)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                vibrator.vibrate(VibrationEffect.createWaveform(pattern, 0))
            } else {
                vibrator.vibrate(pattern, 0)
            }
        }
    }

    private fun playAlarm() {
        val alarmUri = when {
            alarm.sound.isNullOrBlank() -> RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM)
            else -> Uri.parse(alarm.sound)
        }
        Log.d(TAG, "alarm sound: " + alarm.sound)
        Log.d(TAG, "alarm selected: $alarmUri")
        val audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        constDeviceVolume = audioManager.getStreamVolume(AudioManager.STREAM_RING)
        //if (alarmUri == null) alarmUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        //ringtone = RingtoneManager.getRingtone(baseContext, alarmUri)
        val maxVolume = 100.0
        val currVolume = alarm.volume //alarm.volume
        mMediaPlayer.reset()
        mMediaPlayer.setDataSource(this, alarmUri)

        if (constDeviceVolume != 0) {
            //Log.d(TAG, "set up music")
            val log1 = (1 - (Math.log(maxVolume - currVolume) / Math.log(maxVolume))).toFloat()
            Log.d(TAG, "set up music log: $log1")
            mMediaPlayer.setVolume(log1, log1) //set volume takes two paramater
            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_RING)
            mMediaPlayer.isLooping = true
            try {
                mMediaPlayer.prepare()
                mMediaPlayer.start()
            } catch (e: Exception) {
                e.printStackTrace()
                mMediaPlayer.reset()
                mMediaPlayer.setDataSource(this, RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM))
                mMediaPlayer.prepare()
                mMediaPlayer.start()
            }
        }
    }

    private val checkCurrentTopActivityTask = object : Runnable {
        override fun run() {
            mainHandler.postDelayed(this, 5000)
            Log.d(TAG, "run()")
            if (!isShutdownDefault && !isShutdownShake && !isShutdownGame) return
            // TODO test it
            setVibration()
            val am = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            val taskInfo = am.getRunningTasks(1)
            Log.d("topActivity", "CURRENT Activity: " + taskInfo[0].topActivity.className)
            // TODO taskInfo[0].topActivity.javaClass.simpleName to get simplest name
            if (taskInfo[0].topActivity.className != "com.michael.alarmclock.alarm.DefaultStopActivity" &&
                taskInfo[0].topActivity.className != "com.michael.alarmclock.alarm.ShakeStopActivity" &&
                taskInfo[0].topActivity.className != "com.michael.alarmclock.alarm.GameStopActivity") {
                Log.d(TAG, "fire activity from run()")
                fireActivity()
            }
        }
    }


    private val broadcastReceiver = object : BroadcastReceiver() {
        var wasVolumeChangedByApp = false
        override fun onReceive(context: Context, intent: Intent) {

            if ("android.media.VOLUME_CHANGED_ACTION" == intent.action && !wasVolumeChangedByApp) {
                val audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
                audioManager.setStreamVolume(AudioManager.STREAM_RING, constDeviceVolume, 0)
                Log.d(TAG, "changed by me")
                wasVolumeChangedByApp = true
                return
            }
            wasVolumeChangedByApp = false
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy")
        unregisterReceiver(broadcastReceiver)
        mainHandler.removeCallbacks(checkCurrentTopActivityTask)

//        mMediaPlayer.stop()
        vibrator.cancel()
        mMediaPlayer.release()
    }

    private fun fireActivity() {

        var cls = when {
            isShutdownGame && gameLevel > 0 && gameTime >= 5000 -> GameStopActivity::class.java
            isShutdownShake && shakeCount > 0 -> ShakeStopActivity::class.java
            else -> DefaultStopActivity::class.java
        }

        var intent = Intent(this, cls)
        intent.putExtra("alarm", alarm)
        intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        //Log.i(TAG, "startActivity")
        startActivity(intent)
    }

    private val locationListener: LocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            Log.i(TAG, "onLocationChanged() in listener location lon: ${location.longitude} lat: ${location.latitude}")
            Log.d("Location Changes", location.toString())

            var intent = Intent(context, WeatherActivity::class.java)
            intent.putExtra("location", location)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }

        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {
            Log.d("Status Changed", status.toString())
        }

        override fun onProviderEnabled(provider: String) {
            Log.d("Provider Enabled", provider)
        }

        override fun onProviderDisabled(provider: String) {
            Log.d("Provider Disabled", provider)
        }
    }

    private fun showWeatherActivity() {
        // Now first make a criteria with your requirements
        // this is done to save the battery life of the device
        // there are various other other criteria you can search for..
        val criteria = Criteria()


        // Now create a location manager
        val locationManager by lazy { getSystemService(Context.LOCATION_SERVICE) as LocationManager }

        // This is the Best And IMPORTANT part
        val looper: Looper? = null

        criteria.accuracy = Criteria.ACCURACY_COARSE
        criteria.powerRequirement = Criteria.POWER_LOW
        criteria.isAltitudeRequired = false
        criteria.isBearingRequired = false
        criteria.isSpeedRequired = false
        criteria.isCostAllowed = true
        criteria.horizontalAccuracy = Criteria.ACCURACY_HIGH
        criteria.verticalAccuracy = Criteria.ACCURACY_HIGH

        locationManager.requestSingleUpdate(criteria, locationListener, looper)
    }

    private fun checkLocationPermission(): Boolean {

        return PermissionChecker.checkSelfPermission(
            applicationContext,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    }
}