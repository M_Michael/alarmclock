package com.michael.alarmclock.alarm.util

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Build.VERSION.SDK_INT
import android.util.Log
import com.michael.alarmclock.alarm.AlarmReceiver
import com.michael.alarmclock.data.alarm.Alarm
import java.util.*
import java.io.Serializable
import android.os.Build.VERSION.SDK_INT
import android.os.Bundle
import com.google.gson.Gson
import androidx.core.view.accessibility.AccessibilityEventCompat.setAction
import com.michael.alarmclock.alarm.App


fun addAlarm(context: Context, calendar: Calendar, alarm: Alarm) : Int {

    //var alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
    val myIntent = Intent(context, AlarmReceiver::class.java)
    if (alarm.id == 0) alarm.id = System.currentTimeMillis().toInt()
    var startUpTime: Long
    if (!alarm.daysToRepeat.isNullOrEmpty()) {
        for(day_id in alarm.daysToRepeat) {
            //Log.i("day id: ", "Days: " + day_id.toString())
            when (day_id) {
                1 -> calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY)
                2 -> calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)
                3 -> calendar.set(Calendar.DAY_OF_WEEK, Calendar.TUESDAY)
                4 -> calendar.set(Calendar.DAY_OF_WEEK, Calendar.WEDNESDAY)
                5 -> calendar.set(Calendar.DAY_OF_WEEK, Calendar.THURSDAY)
                6 -> calendar.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY)
                7 -> calendar.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY)
            }
            startUpTime = calendar.timeInMillis
            if (calendar.timeInMillis < Date().time) startUpTime = calendar.timeInMillis + AlarmManager.INTERVAL_DAY * 7
            alarm.timeInMillis = startUpTime

            myIntent.putExtra("alarmId", alarm.id)

            val pendingIntent = PendingIntent.getBroadcast(context, alarm.id + day_id, myIntent, PendingIntent.FLAG_UPDATE_CURRENT)
            setAlarm(context, startUpTime, pendingIntent)
            Log.i("addAlarm", "alarm with ID ${alarm.id + day_id} was added")
        }
    } else {
        if (calendar.timeInMillis < Date().time) calendar.timeInMillis += AlarmManager.INTERVAL_DAY
        alarm.timeInMillis = calendar.timeInMillis

        myIntent.putExtra("alarmId", alarm.id)

        Log.i("addAlarm", "alarm: $alarm")
        val pendingIntent = PendingIntent.getBroadcast(context, alarm.id, myIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        setAlarm(context, calendar.timeInMillis, pendingIntent)
        Log.i("addAlarm", "alarm with ID ${alarm.id} was added")
    }
    return alarm.id
}

fun deleteAlarm(context: Context, alarm: Alarm) {
    val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
    var myIntent = Intent(context, AlarmReceiver::class.java)

    //Log.i("alarm in helper: ", alarm.toString())
    if (alarm.isRepeating && !alarm.daysToRepeat.isNullOrEmpty()) {
        for (day_id in alarm.daysToRepeat) {
            val pendingIntent =
                PendingIntent.getBroadcast(context, alarm.id + day_id, myIntent, PendingIntent.FLAG_UPDATE_CURRENT)
            alarmManager.cancel(pendingIntent)
            Log.i("deleteAlarm", "alarm with ID ${alarm.id + day_id} was deleted")
        }
    } else {
        val pendingIntent = PendingIntent.getBroadcast(context, alarm.id, myIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        alarmManager.cancel(pendingIntent)
        Log.i("deleteAlarm", "alarm with ID ${alarm.id} was deleted")
    }
}

fun setAlarm(context: Context, time: Long, pendingIntent: PendingIntent) {
    val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
    //alarmManager.setExact(AlarmManager.RTC_WAKEUP, time, pendingIntent)
    if (Build.VERSION_CODES.KITKAT <= SDK_INT && SDK_INT < Build.VERSION_CODES.M)
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, time, pendingIntent)
    else if (SDK_INT >= Build.VERSION_CODES.M)
        alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, time, pendingIntent)
}