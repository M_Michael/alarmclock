package com.michael.alarmclock.alarm

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import com.michael.alarmclock.R
import java.lang.ClassCastException
import android.view.inputmethod.InputMethodManager
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat.getSystemService
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.AdView
import kotlinx.android.synthetic.main.ad_recycler_item.view.*
import kotlinx.android.synthetic.main.dialog_exit.view.*


class AlarmExitDialog : DialogFragment() {

    var description: String? = ""
    lateinit var btnOK: Button
    lateinit var btnBack: Button

    private val TAG = "AlarmExitDialog"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var myView = inflater.inflate(R.layout.dialog_exit, container, false)

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        setStyle(STYLE_NO_FRAME, 0)
        super.onCreateView(inflater, container, savedInstanceState)

        btnOK = myView.findViewById(R.id.exit_ok)
        btnBack = myView.findViewById(R.id.exit_back)

        btnOK.setOnClickListener{
            activity!!.finishAndRemoveTask()
        }

        btnBack.setOnClickListener{
            dismiss()
        }

        //val adRequest = PublisherAdRequest.Builder().build()
        val adContainer = myView.exit_add
        //val customAdSize = AdSize(holder.view.alarm_card.width, holder.view.alarm_card.height)
        val customAdSize = AdSize(360, 180)
        val mAdView = AdView(context)
        mAdView.adSize = AdSize.MEDIUM_RECTANGLE
        mAdView.adUnitId = "ca-app-pub-6317774061864467/9639001622" //ca-app-pub-3940256099942544/6300978111 /6499/example/banner ca-app-pub-3940256099942544/2247696110
        (adContainer as RelativeLayout).addView(mAdView)
        val adRequest = AdRequest.Builder().addTestDevice("35878B09648A844132EDAAAB1D5C113D").build()
        mAdView.loadAd(adRequest)

        return myView
    }

}