package com.michael.alarmclock.alarm

import android.app.PendingIntent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.michael.alarmclock.R
import com.squareup.seismic.ShakeDetector
import android.hardware.SensorManager
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.michael.alarmclock.alarm.service.NotificationService
import com.michael.alarmclock.alarm.util.setAlarm
import com.michael.alarmclock.data.alarm.Alarm
import java.util.*


class ShakeStopActivity: AppCompatActivity(), ShakeDetector.Listener {

    private val TAG = "ShakeStopActivity"
    private var shakeCount: Int = 0
    private val shakeText by lazy { findViewById<TextView>(R.id.shakes_left_text) }
    private val shakeDetector by lazy { ShakeDetector(this) }
    private var isDisabled = false
    private var isSkipped = false
    private val alarm by lazy { intent.getParcelableExtra<Alarm>("alarm") }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(TAG, "onCreate()")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.stop_activity_shake_alarm)

        window.addFlags(
            WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON or
            WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD or
            WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED or
            WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
        )

        if (savedInstanceState != null) {
            //Log.d(TAG, "shake: $shakeCount")
            shakeCount = savedInstanceState.getInt("shakeCount")
        } else {
            shakeCount = alarm.shakeCount
        }

        val btnSkip = findViewById<Button>(R.id.button_skip_shake)
        btnSkip.text = "Skip alarm for ${alarm.skippingDelay / 60000} min"
        if (alarm.allowedSkippingTimes > 0) {
            btnSkip.visibility = View.VISIBLE
        } else {
            btnSkip.visibility = View.GONE
        }
        btnSkip.setOnClickListener {
            isSkipped = true
            alarm.allowedSkippingTimes --
            this@ShakeStopActivity.finishAndRemoveTask()
        }

        val shakeDesc = findViewById<TextView>(R.id.shake_alarm_description)

        shakeText.text = "$shakeCount"
        shakeDesc.text = alarm.description

        val sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        shakeDetector.start(sensorManager)
    }

    override fun hearShake() {
        shakeCount --
        if (shakeCount >= 1)
            shakeText.text = "$shakeCount"
        else {
            isDisabled = true

            App.setOffOutOfDateAlarms(this)

            shakeDetector.stop()
            this@ShakeStopActivity.finishAndRemoveTask()
        }
    }

    override fun onDestroy() {
        if (!isSkipped) {
            val serviceIntent = Intent(this, NotificationService::class.java)
            serviceIntent.putExtra("isSkipped", isSkipped)
            serviceIntent.putExtra("isShakeStopped", isDisabled)
            serviceIntent.putExtra("alarm", alarm)
            ContextCompat.startForegroundService(this, serviceIntent)
        } else {
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = Date().time + alarm.skippingDelay
            Log.d(TAG, "skip for: " + alarm.skippingDelay.div(60000) + " mins")

            val myIntent = Intent(this.applicationContext, AlarmReceiver::class.java)
            //myIntent.putExtra("isSkipping", true)
            Log.d(TAG, "alarm: $alarm")
            myIntent.putExtra("alarmId", alarm.id)
            myIntent.putExtra("skipLeft", alarm.allowedSkippingTimes)
            myIntent.action = App.SKIPPED
            val pendingIntent =
                PendingIntent.getBroadcast(this.applicationContext, alarm.id, myIntent, PendingIntent.FLAG_UPDATE_CURRENT)
            setAlarm(this.applicationContext, calendar.timeInMillis, pendingIntent)

            val serviceIntent = Intent(this, NotificationService::class.java)
            serviceIntent.putExtra("isSkipped", isSkipped)
            serviceIntent.putExtra("alarm", alarm)
            ContextCompat.startForegroundService(this, serviceIntent)
        }
        shakeDetector.stop()
        super.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        outState!!.putInt("shakeCount", shakeCount)
    }
}