package com.michael.alarmclock.alarm

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.michael.alarmclock.R
import com.wajahatkarim3.easyflipview.EasyFlipView
import com.shawnlin.numberpicker.NumberPicker
import androidx.appcompat.widget.SwitchCompat
import androidx.cardview.widget.CardView
import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.MenuItem
import android.widget.*


class ShutdownTypeActivity : AppCompatActivity() {

    private val cardDefault by lazy { findViewById<EasyFlipView>(R.id.shutdown_type_default_card) }
    private val cardShake   by lazy { findViewById<EasyFlipView>(R.id.shutdown_type_shake_card) }
    private val cardGame    by lazy { findViewById<EasyFlipView>(R.id.shutdown_type_game_card) }
    private val TAG         = "ShutdownTypeActivity"
    private var isDefault   = true
    private var isShake     = false
    private var shakeCount  = 0
    private var isGame      = false
    private var gameLevel   = 1
    private var gameTime    = 5

    private val shakeStep = 5

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shutdown_types)

        val toolbar = findViewById<androidx.appcompat.widget.Toolbar >(R.id.shutdown_toolbar)
        toolbar.title = "Shutdown types"

        if(intent.extras != null && !intent.extras.isEmpty) {
            isDefault = intent.getBooleanExtra("isDefault", true)
            isShake = intent.getBooleanExtra("isShake", false)
            shakeCount = intent.getIntExtra("shakeCount", 0)
            if (shakeCount != 0) shakeCount /= 5
            isGame = intent.getBooleanExtra("isGame", false)
            gameLevel = intent.getIntExtra("gameLevel", 1)
            gameTime = (intent.getLongExtra("gameTime", 5000) / 1000L).toInt()
        }

        /** Default start*/
        val defaultFront = findViewById<CardView>(R.id.shutdown_type_shake_card_front)
        val defaultTextView = findViewById<TextView>(R.id.default_type_text)
        val defaultSwitcher = findViewById<SwitchCompat>(R.id.shutdown_default_switcher)

        cardDefault.isAutoFlipBack = false
        defaultFront.isClickable = true
        defaultFront.setOnClickListener {
            cardDefault.flipTheView()
        }
        defaultTextView.setOnClickListener {
            cardDefault.flipTheView()
        }
        defaultSwitcher.isChecked = isDefault
        defaultSwitcher.setOnClickListener {
            isDefault = defaultSwitcher.isChecked
        }

        /** Default end*/

        /** Shake start*/
        val btnShakeOk = findViewById<ImageButton>(R.id.shake_ok)
        val btnShakeCancel = findViewById<ImageButton>(R.id.shake_cancel)
        val btnShakeAdd = findViewById<Button>(R.id.shake_add_count)
        val btnShakeMinus = findViewById<Button>(R.id.shake_minus_count)
        val shakeCountPicker = findViewById<NumberPicker>(R.id.shake_count_picker)
        val shakeFront = findViewById<CardView>(R.id.shutdown_type_shake_card_front)
        val shakeSwitcher = findViewById<SwitchCompat>(R.id.shutdown_shake_switcher)

        updateShakesText()

        shakeSwitcher.isChecked = isShake
        shakeSwitcher.setOnClickListener {
            isShake = shakeSwitcher.isChecked
        }
        shakeFront.isClickable = true
        cardShake.isAutoFlipBack = false

        shakeFront.setOnClickListener {
            cardShake.flipTheView()
        }

        val formatter = NumberPicker.Formatter { value ->
            val diff = (value * shakeStep)
            "" + diff
        }
        shakeCountPicker.formatter = formatter
        shakeCountPicker.isClickable = true
        shakeCountPicker.value = shakeCount

        btnShakeOk.setOnClickListener {
            shakeCount = shakeCountPicker.value
            updateShakesText()
            cardShake.flipTheView()
        }
        btnShakeCancel.setOnClickListener {
            shakeCountPicker.value = 0
            cardShake.flipTheView()
        }
        btnShakeAdd.setOnClickListener {
            shakeCountPicker.value += 2
        }
        btnShakeMinus.setOnClickListener {
            shakeCountPicker.value -= 2
        }
        /** Shake end*/

        /** Game start*/
        val gameFront = findViewById<CardView>(R.id.shutdown_type_game_card_front)
        val gameBack = findViewById<CardView>(R.id.shutdown_type_game_card_back)
        val gameSwitcher = findViewById<SwitchCompat>(R.id.shutdown_game_switcher)
        val gameTimePicker = findViewById<NumberPicker>(R.id.game_time_picker)
        val gameLevelRadio = findViewById<RadioGroup>(R.id.game_level_radio_group)

        updateGameText()

        gameSwitcher.isChecked = isGame
        gameSwitcher.setOnClickListener {
            isGame = gameSwitcher.isChecked
        }
        gameFront.isClickable = true
        cardGame.isAutoFlipBack = false

        gameFront.setOnClickListener {
            cardGame.flipTheView()
        }
        gameBack.setOnClickListener {
            cardGame.flipTheView()
        }

        val time = arrayOf("5", "10", "15", "20", "25", "30")
        gameTimePicker.minValue = 1
        gameTimePicker.wrapSelectorWheel = false
        gameTimePicker.maxValue = time.size
        gameTimePicker.displayedValues = time
        gameTimePicker.value = gameTime / 5
        gameTimePicker.setOnValueChangedListener { _, _, newVal ->
            gameTime = time[newVal - 1].toInt()
            updateGameText()
        }

        gameLevelRadio.setOnCheckedChangeListener { _, checkedId ->
            gameLevel = when (checkedId) {
                R.id.lvl1 -> 1
                R.id.lvl2 -> 2
                R.id.lvl3 -> 3
                else -> 1
            }
            updateGameText()
        }
        /** Game end*/

        val btn_ok = findViewById<Button>(R.id.button_ok)
        btn_ok.setOnClickListener {
            val resultIntent = Intent()
            resultIntent.putExtra("isDefault", isDefault)
            resultIntent.putExtra("isShake", isShake)
            resultIntent.putExtra("shakeCount", shakeCount * 5)
            resultIntent.putExtra("isGame", isGame)
            resultIntent.putExtra("gameLevel", gameLevel)
            resultIntent.putExtra("gameTime", (gameTime * 1000).toLong())
            setResult(Activity.RESULT_OK, resultIntent)
            this@ShutdownTypeActivity.finish()
        }

        val btn_back = findViewById<Button>(R.id.button_back)
        btn_back.setOnClickListener {
            val resultIntent = Intent()
            setResult(Activity.RESULT_CANCELED, resultIntent)
            this@ShutdownTypeActivity.finish()
        }

    }

    private fun updateShakesText() {
        val shakeOptionsText = findViewById<TextView>(R.id.shutdown_shake_options)
        shakeOptionsText.text = if (shakeCount > 0) "Shakes: ${shakeCount * shakeStep}" else ""
    }

    private fun updateGameText() {
        val gameOptionsText = findViewById<TextView>(R.id.shutdown_game_options)
        gameOptionsText.text = "Level $gameLevel for $gameTime sec"
    }

}