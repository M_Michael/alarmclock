package com.michael.alarmclock.alarm

import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.airbnb.lottie.LottieAnimationView
import com.michael.alarmclock.R

class PreGameFragment: Fragment() {

    private val mActivity by lazy { activity as (GameStopActivity) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_pre_game, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val btnLowLevelStart = view.findViewById<Button>(R.id.button_low_level)
        btnLowLevelStart.setOnClickListener {
            if (GameStopActivity.level > 1)
                GameStopActivity.level --
            mActivity.changeFragment("game")
        }

        if (GameStopActivity.isRetry) {
            val txtAction = view.findViewById<TextView>(R.id.text_action)
            txtAction.text = "RETRY"
        } else {
            btnLowLevelStart.visibility = View.INVISIBLE
        }

        val txtInfo = view.findViewById<TextView>(R.id.text_info)
        txtInfo.text = Html.fromHtml("Current speed level: <b>${GameStopActivity.level}</b>")

        val btnStart = view.findViewById<LottieAnimationView>(R.id.button_active)
        btnStart.setOnClickListener {
            mActivity.changeFragment("game")
        }

        val btnSkip = view.findViewById<Button>(R.id.button_skip_game)
        btnSkip.text = "Skip alarm for ${GameStopActivity.currentAlarm.skippingDelay / 60000} min"
        if (GameStopActivity.currentAlarm.allowedSkippingTimes > 0) {
            btnSkip.visibility = View.VISIBLE
        } else {
            btnSkip.visibility = View.GONE
        }
        btnSkip.setOnClickListener {
            GameStopActivity.isSkipped = true
            GameStopActivity.currentAlarm.allowedSkippingTimes --
            mActivity.finish()
        }

    }
}