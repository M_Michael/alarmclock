package com.michael.alarmclock.alarm

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.michael.alarmclock.data.alarm.Alarm
import com.michael.alarmclock.data.alarm.AlarmRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class AlarmViewModel(application: Application) : AndroidViewModel(application) {

    private var parentJob = Job()
    // By default all the coroutines launched in this scope should be using the Main dispatcher
    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.Main
    private val scope = CoroutineScope(coroutineContext)

    private val repository: AlarmRepository
    // Using LiveData and caching what getAlphabetizedAlarms returns has several benefits:
    // - We can put an observer on the data (instead of polling for changes) and only update the
    //   the UI when the data actually changes.
    // - Repository is completely separated from the UI through the ViewModel.
    val allAlarms: LiveData<List<Alarm>>

    init {
        val AlarmsDao = AppDatabase.getDatabase(application, scope).alarmDao()
        repository = AlarmRepository(AlarmsDao)
        allAlarms = repository.getAlarmsLiveData()
    }

    /**
     * Launching a new coroutine to insert the data in a non-blocking way
     */
    fun insert(Alarm: Alarm) = scope.launch(Dispatchers.IO) {
        repository.saveAlarm(Alarm)
    }

    fun deleteAlarm(alarm: Alarm) = repository.deleteAlarm(alarm)

    fun getAlarmsLiveData(): LiveData<List<Alarm>> = allAlarms

    fun getAlarms(): List<Alarm> = repository.getAlarms()

    fun getAlarmById(id: Int): Alarm = repository.getAlarm(id)

    fun updateAlarm(alarm: Alarm) = repository.saveAlarm(alarm)

    override fun onCleared() {
        super.onCleared()
        parentJob.cancel()
    }
}