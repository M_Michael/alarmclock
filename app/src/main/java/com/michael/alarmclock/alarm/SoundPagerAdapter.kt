package com.michael.alarmclock.alarm

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class SoundPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                SoundRingtoneFragment()
            }
            1 -> SoundAppFragment()
            else -> {
                return SoundMyMusicFragment()
            }
        }
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0 -> "Ringtone"
            1 -> "App"
            else -> {
                return "Music"
            }
        }
    }
}