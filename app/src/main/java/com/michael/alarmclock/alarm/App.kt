package com.michael.alarmclock.alarm

import android.app.Activity
import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.util.Log
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProviders
import java.util.*


class App : Application() {

    private val TAG = "App"

    companion object {
        const val CHANNEL_ID        = "exampleServiceChannel"
        const val SHARED_PREFS      = "sharedPrefs"
        const val SWITCH_WEATHER    = "switchWeather"
        const val TIME_FORMAT       = "timeFormat"
        const val SKIPPED           = "skipped"
        const val DISABLE_VOLUME    = "disableVolume"

        fun setOffOutOfDateAlarms(fragment: FragmentActivity) {
            val mAlarmViewModel = ViewModelProviders.of(fragment)[AlarmViewModel::class.java]
            val alarmsList = mAlarmViewModel.getAlarms()
            if (!alarmsList.isNullOrEmpty()) {
                for (alarm in alarmsList) {
                    if (!alarm.isRepeating && alarm.timeInMillis < Date().time) {
                        alarm.enabled = false
                        mAlarmViewModel.insert(alarm)
                    }
                }
            }
        }
    }
}