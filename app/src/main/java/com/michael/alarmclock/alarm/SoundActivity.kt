package com.michael.alarmclock.alarm

import android.app.Activity
import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.michael.alarmclock.R
import kotlinx.android.synthetic.main.activity_sound.*
import com.michael.alarmclock.MainActivity
import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.app.AlertDialog
import androidx.core.app.ActivityCompat
import android.content.pm.PackageManager
import android.os.Build
import android.widget.Button
import androidx.core.content.PermissionChecker

class SoundActivity : AppCompatActivity(), SoundListAdapter.SendSoundUri  {

    override fun sendSoundUri(input: String?) {
        Log.i(TAG, "sendSoundUri(): got the input: $input")
        soundUri = input
    }

    private var soundUri: String? = ""
    private val TAG = "SoundActivity"

    companion object {
        val mMediaPlayer = MediaPlayer()
        var isStoragePermission = false
        var previousFragment = 0
        var currentUsingFragment = 0
        var currentPlayingRingtoneMusic  = -1
        var currentPlayingAppMusic       = -1
        var currentPlayingMyMusic        = -1
        lateinit var myMusicAdapter:  SoundListAdapter
        lateinit var appMusicAdapter: SoundListAdapter
        lateinit var ringtoneAdapter: SoundListAdapter
        fun updateAdapter() {

            when (currentUsingFragment) {
                0 -> {
                    currentPlayingAppMusic = -1
                    currentPlayingMyMusic = -1
                    if (::myMusicAdapter.isInitialized)
                        myMusicAdapter.notifyDataSetChanged()
                    appMusicAdapter.notifyDataSetChanged()

                }
                1 -> {
                    currentPlayingRingtoneMusic = -1
                    currentPlayingMyMusic = -1
                    ringtoneAdapter.notifyDataSetChanged()
                    if (::myMusicAdapter.isInitialized)
                        myMusicAdapter.notifyDataSetChanged()
                }
                2 -> {
                    currentPlayingRingtoneMusic = -1
                    currentPlayingAppMusic = -1
                    ringtoneAdapter.notifyDataSetChanged()
                    appMusicAdapter.notifyDataSetChanged()
                }
            }
            previousFragment = currentUsingFragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        MainActivity.isFragmentSwitch = false

        mMediaPlayer.reset()

        setContentView(R.layout.activity_sound)
        val toolbar = findViewById<androidx.appcompat.widget.Toolbar>(R.id.sound_toolbar)
        toolbar.title = "Sounds"

        isStoragePermissionGranted()
        val fragmentAdapter = SoundPagerAdapter(supportFragmentManager)
        val viewPager = findViewById<ViewPager>(R.id.viewpager_main)
        viewPager.adapter = fragmentAdapter
        tabs_main.setupWithViewPager(viewPager)


        val btn_ok = findViewById<Button>(R.id.button_ok)
        btn_ok.setOnClickListener {
            val resultIntent = Intent()
            Log.d(TAG, "sending uri: $soundUri")
            resultIntent.putExtra("soundUri", soundUri)
            setResult(Activity.RESULT_OK, resultIntent)
            this@SoundActivity.finish()
        }

        val btn_back = findViewById<Button>(R.id.button_back)
        btn_back.setOnClickListener {
            val resultIntent = Intent()
            setResult(Activity.RESULT_CANCELED, resultIntent)
            this@SoundActivity.finish()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy()")
        try {
            if (SoundActivity.mMediaPlayer.isPlaying) {
                Log.d(TAG, "onDestroy(): HERE")
                SoundActivity.mMediaPlayer.stop()
            }
            //SoundActivity.mMediaPlayer.release()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        }
    }


    private fun isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {

            if (PermissionChecker.checkSelfPermission(
                    this,
                    READ_EXTERNAL_STORAGE
                ) == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted")
                isStoragePermission = true
            } else {
                Log.v(TAG, "Permission is revoked")
                AlertDialog.Builder(this)
                    .setTitle("Permission needed")
                    .setMessage("This permission is needed for using your music as alarm ringtone.")
                    .setPositiveButton("Ok") { _, _ ->
                        ActivityCompat.requestPermissions(this, arrayOf(READ_EXTERNAL_STORAGE), 1)
                    }
                    .setNegativeButton("Cancel") { dialog, _ ->
                        dialog.dismiss()
                        isStoragePermission = false
                    }
                    .create().show()
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted")
            isStoragePermission = true
        }
    }
}