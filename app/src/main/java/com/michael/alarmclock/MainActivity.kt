package com.michael.alarmclock

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.content.Context
import android.os.PersistableBundle
import android.util.Log
import com.google.android.gms.ads.doubleclick.PublisherAdView
import android.view.WindowManager
import android.util.DisplayMetrics
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.michael.alarmclock.alarm.*
import com.michael.alarmclock.settings.SettingsFragment


class MainActivity : AppCompatActivity(), AutoStartPermissionDialog.PermissionListener {

    private val TAG = "MainActivity"
    override fun sendInput(permission: Boolean) {
        autostartPermission = permission

        val settings = applicationContext.getSharedPreferences("AUTOSTART", Context.MODE_PRIVATE)
        val editor = settings.edit()
        editor.putBoolean("isAutoStartPermissionGuaranteed", autostartPermission)
        Log.d(TAG, "applying autostart to: $autostartPermission")
        editor.apply()
    }

    companion object {
        var screenWidth = 0
        var isFragmentSwitch = true
        var timeFormat = 24
    }

    private var autostartPermission = false
    private lateinit var currentFragment: String
    //lateinit var mPublisherAdView : PublisherAdView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if ("xiaomi".equals(android.os.Build.MANUFACTURER, true) ||
            "letv".equals(android.os.Build.MANUFACTURER, true)) {
            // Get from the SharedPreferences
            val settings = applicationContext.getSharedPreferences("AUTOSTART", Context.MODE_PRIVATE)
            val isAutoStartPermissionGuaranteed = settings.getBoolean("isAutoStartPermissionGuaranteed", false)
            if (!isAutoStartPermissionGuaranteed) {
                val alarmDialog = AutoStartPermissionDialog()
                val fm = supportFragmentManager
                alarmDialog.show(fm, "MyDialog")
            }
        }

        App.setOffOutOfDateAlarms(this)
        updateTimeFormat()
        if (savedInstanceState != null) {
            //Log.d(TAG, "savedInstanceState: ${savedInstanceState.getString("currentFragment")}")
            when (savedInstanceState.getString("currentFragment")) {
                "class com.michael.alarmclock.alarm.AlarmsRecyclerFragment"    -> changeFragment(AlarmsRecyclerFragment())
                "class com.michael.alarmclock.settings.SettingsFragment"       -> changeFragment(SettingsFragment())
                "class com.michael.alarmclock.alarm.AlarmEditFragment"         -> changeFragment(AlarmEditFragment())
                else -> changeFragment(AlarmsRecyclerFragment())
            }
        } else {
            currentFragment = AlarmsRecyclerFragment().javaClass.toString()
            changeFragment(AlarmsRecyclerFragment())
        }
        // TODO move this code to alarm activity
        //Log.d(TAG, "margin: ${resources.getDimension(R.dimen.alarm_item_recycle_side_margin).toInt()}")
        screenWidth = getScreenHeightInDPs(this) - resources.getDimension(R.dimen.alarm_item_recycle_side_margin).toInt()
    }

    override fun onBackPressed() {
        //super.onBackPressed()savedInstanceState
        Log.i(TAG, "currentFragment: $currentFragment")
        (AlarmsRecyclerFragment() as? IOnBackPressed)?.onBackPressed()?.let {
            Log.d(TAG, "HERE 1: " + AlarmsRecyclerFragment().javaClass.toString())
            //super.onBackPressed()
        }
        if (currentFragment == AlarmsRecyclerFragment().javaClass.toString()) {
            val alarmDialog = AlarmExitDialog()
            alarmDialog.show(supportFragmentManager, "MyDialog")
        } else {
            changeFragment(AlarmsRecyclerFragment())
        }

    }
    private fun getScreenHeightInDPs(context: Context): Int {
        /*
            DisplayMetrics
                A structure describing general information about a display,
                such as its size, density, and font scaling.
        */
        val dm = DisplayMetrics()

        val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        windowManager.defaultDisplay.getMetrics(dm)
        return Math.round(dm.widthPixels / dm.density)
    }

    interface IOnBackPressed {
        fun onBackPressed()
    }

    fun changeFragment(changeTo: Fragment) {
        updateTimeFormat()
        currentFragment = changeTo.javaClass.toString()
        Log.d(TAG, "currentFragment: $currentFragment")
        replaceFragment(changeTo, R.id.container)
    }

    private inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> Unit) {
        val fragmentTransaction = beginTransaction()
        fragmentTransaction.func()
        fragmentTransaction.commit()
    }

    private fun AppCompatActivity.addFragment(fragment: Fragment, frameId: Int){
        supportFragmentManager.inTransaction { add(frameId, fragment) }
    }


    private fun AppCompatActivity.replaceFragment(fragment: Fragment, frameId: Int) {
        supportFragmentManager.inTransaction{replace(frameId, fragment)}
    }

    fun updateTimeFormat() {
        val sharedPreferences = getSharedPreferences(
            App.SHARED_PREFS,
            Context.MODE_PRIVATE
        )
        timeFormat = sharedPreferences.getInt(App.TIME_FORMAT, 24)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState!!.putString("currentFragment", currentFragment)
    }

}
