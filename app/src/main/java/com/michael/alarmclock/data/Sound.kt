package com.michael.alarmclock.data

// Initialize a new data class to hold audio data
data class Sound(val title: String, val uri: String)