package com.michael.alarmclock.data

import com.michael.alarmclock.R

private val arrayMusicRes = arrayListOf(
        R.raw.sound_1,
        R.raw.sound_2
    )

fun getAppSounds(): ArrayList<Sound> {
    val list: ArrayList<Sound> = arrayListOf()
    for (i in arrayMusicRes.indices)
        list.add(Sound("Sound ${i + 1}", "android.resource://com.michael.alarmclock/${arrayMusicRes[i]}"))
    return list
}

fun getAppSoundNameByUri(uri: String): String? =
    getAppSounds().findLast { sound -> sound.uri == uri }?.title