package com.michael.alarmclock.data.alarm

import android.util.Log
import androidx.lifecycle.LiveData
import org.jetbrains.anko.doAsync
import java.util.concurrent.Callable
import java.util.concurrent.Executors


class AlarmRepository (private val alarmDao: AlarmDao): AlarmDataSource {
    private val TAG = "AlarmRepository"
    override fun getAlarmsLiveData(): LiveData<List<Alarm>> {
        return alarmDao.getAllLiveData()
    }

    override fun getAlarms(): List<Alarm> {
        val callable = object : Callable<List<Alarm>> {
            override fun call(): List<Alarm> {
                val list = alarmDao.getAll()
                Log.i(TAG, "getAlarms")
                return list.sortedWith(compareBy({ it.hour }, { it.min }))
            }
        }

        val future = Executors.newSingleThreadExecutor().submit(callable)

        return future.get()

    }

//    override fun getAlarm(id: Int): Alarm {
//        return alarmDao.getById(id)
//    }

    override fun getAlarm(id: Int): Alarm {

        val callable = object : Callable<Alarm> {
            override fun call(): Alarm {
                return alarmDao.getById(id)
            }
        }

        val future = Executors.newSingleThreadExecutor().submit(callable)

        return future.get()
    }

    override fun saveAlarm(alarm: Alarm) {
        doAsync { alarmDao.insert(alarm) }
    }

    override fun deleteAlarm(alarm: Alarm) {
        doAsync { alarmDao.delete(alarm) }
    }

}
