package com.michael.alarmclock.data.alarm

import androidx.lifecycle.LiveData
import androidx.room.*
import com.michael.alarmclock.data.alarm.Alarm


@Dao
interface AlarmDao {

    @Query("SELECT * FROM alarm_table")
    fun getAllLiveData(): LiveData<List<Alarm>>

    @Query("SELECT * FROM alarm_table")
    fun getAll(): List<Alarm>

    @Query("SELECT * FROM alarm_table WHERE id = :id LIMIT 1")
    fun getById(id: Int): Alarm

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(alarm: Alarm)

    @Delete
    fun delete(alarm: Alarm)
}