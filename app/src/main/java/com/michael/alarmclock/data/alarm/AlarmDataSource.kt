package com.michael.alarmclock.data.alarm

import androidx.lifecycle.LiveData
import com.michael.alarmclock.data.alarm.Alarm

interface AlarmDataSource {
    fun getAlarmsLiveData(): LiveData<List<Alarm>>
    fun getAlarm(id: Int): Alarm
    fun saveAlarm(alarm: Alarm)
    fun deleteAlarm(alarm: Alarm)
    fun getAlarms(): List<Alarm>
}