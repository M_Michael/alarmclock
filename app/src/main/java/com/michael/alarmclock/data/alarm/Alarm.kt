package com.michael.alarmclock.data.alarm

import android.os.Parcel
import android.os.Parcelable
import android.text.Html
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.michael.alarmclock.MainActivity

@Entity(tableName = "alarm_table")
data class Alarm(
    @PrimaryKey
    var id: Int = 0,

    @ColumnInfo(name = "min")
    var min: Int = -1,

    @ColumnInfo(name = "hour")
    var hour: Int = -1,

    @ColumnInfo(name = "timeInMillis")
    var timeInMillis: Long = -1,

    @ColumnInfo(name = "enabled")
    var enabled: Boolean = true,

    @ColumnInfo(name = "daysToRepeat")
    //var daysToRepeat: List<kotlin.Int> = listOf(),
    var daysToRepeat: ArrayList<Int> = arrayListOf(),

    @ColumnInfo(name = "isRepeating")
    var isRepeating: Boolean = false,

    @ColumnInfo(name = "vibration")
    var vibration: Boolean = false,

    @ColumnInfo(name = "isShutdownDefault")
    var isShutdownDefault: Boolean = true,

    @ColumnInfo(name = "isShutdownShake")
    var isShutdownShake: Boolean = false,

    @ColumnInfo(name = "shakeCount")
    var shakeCount: Int = 0,

    @ColumnInfo(name = "isShutdownGame")
    var isShutdownGame: Boolean = false,

    @ColumnInfo(name = "gameLevel")
    var gameLevel: Int = 0,

    @ColumnInfo(name = "gameTime")
    var gameTime: Long = 0,

    @ColumnInfo(name = "description")
    var description: String? = "",

    @ColumnInfo(name = "sound")
    var sound: String? = "",

    @ColumnInfo(name = "volume")
    var volume: Int = 0,

    @ColumnInfo(name = "allowedSkippingTimes")
    var allowedSkippingTimes: Int = 0,

    @ColumnInfo(name = "skippingDelay")
    var skippingDelay: Long = 0): Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readLong(),
        parcel.readByte() != 0.toByte(),
        parcel.readArrayList(Int::class.java.classLoader) as ArrayList<Int>,
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte(),
        parcel.readInt(),
        parcel.readByte() != 0.toByte(),
        parcel.readInt(),
        parcel.readLong(),
        parcel.readString(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readLong()
    )

    fun printTime(): String {
        return if (MainActivity.timeFormat == 24)
                "${if (hour == 24) "00" else hour}:${if (min < 10) "0$min" else min}"
            else {
                if (hour in 13..23)
                    "${hour - 12}:${if (min < 10) "0$min" else min}"
                else
                    "${if (hour == 0) "12" else hour}:${if (min < 10) "0$min" else min}"
            }
    }

    fun printRepeatDays(): String {
        var str: StringBuilder = java.lang.StringBuilder()
        if(daysToRepeat.indexOf(1) != -1) str.append("Sn ")
        if(daysToRepeat.indexOf(2) != -1) str.append("Mn ")
        if(daysToRepeat.indexOf(3) != -1) str.append("Tu ")
        if(daysToRepeat.indexOf(4) != -1) str.append("Wd ")
        if(daysToRepeat.indexOf(5) != -1) str.append("Th ")
        if(daysToRepeat.indexOf(6) != -1) str.append("Fr ")
        if(daysToRepeat.indexOf(7) != -1) str.append("Sa ")
        return str.toString()
    }

    fun printShutdown(): String {
        var listTypes = arrayListOf<String>()
        if (isShutdownDefault) listTypes.add("Default")
        if (isShutdownShake && shakeCount > 0) listTypes.add("Shakes: $shakeCount")
        val separator = ", "
        return listTypes.joinToString { separator }
    }

    fun printDescription() =
        if (description.isNullOrEmpty()) "No title" else description

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeInt(min)
        parcel.writeInt(hour)
        parcel.writeLong(timeInMillis)
        parcel.writeByte(if (enabled) 1 else 0)
        parcel.writeList(daysToRepeat)
        parcel.writeByte(if (isRepeating) 1 else 0)
        parcel.writeByte(if (vibration) 1 else 0)
        parcel.writeByte(if (isShutdownDefault) 1 else 0)
        parcel.writeByte(if (isShutdownShake) 1 else 0)
        parcel.writeInt(shakeCount)
        parcel.writeByte(if (isShutdownGame) 1 else 0)
        parcel.writeInt(gameLevel)
        parcel.writeLong(gameTime)
        parcel.writeString(description)
        parcel.writeString(sound)
        parcel.writeInt(volume)
        parcel.writeInt(allowedSkippingTimes)
        parcel.writeLong(skippingDelay)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Alarm> {
        override fun createFromParcel(parcel: Parcel): Alarm {
            return Alarm(parcel)
        }

        override fun newArray(size: Int): Array<Alarm?> {
            return arrayOfNulls(size)
        }
    }
}
