package com.michael.alarmclock.alarm;

import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.RoomOpenHelper;
import androidx.room.RoomOpenHelper.Delegate;
import androidx.room.util.TableInfo;
import androidx.room.util.TableInfo.Column;
import androidx.room.util.TableInfo.ForeignKey;
import androidx.room.util.TableInfo.Index;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Callback;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Configuration;
import com.michael.alarmclock.data.alarm.AlarmDao;
import com.michael.alarmclock.data.alarm.AlarmDao_Impl;
import java.lang.IllegalStateException;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.HashSet;

@SuppressWarnings("unchecked")
public final class AppDatabase_Impl extends AppDatabase {
  private volatile AlarmDao _alarmDao;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(2) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `alarm_table` (`id` INTEGER NOT NULL, `min` INTEGER NOT NULL, `hour` INTEGER NOT NULL, `timeInMillis` INTEGER NOT NULL, `enabled` INTEGER NOT NULL, `daysToRepeat` TEXT NOT NULL, `isRepeating` INTEGER NOT NULL, `vibration` INTEGER NOT NULL, `isShutdownDefault` INTEGER NOT NULL, `isShutdownShake` INTEGER NOT NULL, `shakeCount` INTEGER NOT NULL, `isShutdownGame` INTEGER NOT NULL, `gameLevel` INTEGER NOT NULL, `gameTime` INTEGER NOT NULL, `description` TEXT, `sound` TEXT, `volume` INTEGER NOT NULL, `allowedSkippingTimes` INTEGER NOT NULL, `skippingDelay` INTEGER NOT NULL, PRIMARY KEY(`id`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"395ce87679cc3ab0f86eec062601c36f\")");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `alarm_table`");
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      protected void validateMigration(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsAlarmTable = new HashMap<String, TableInfo.Column>(19);
        _columnsAlarmTable.put("id", new TableInfo.Column("id", "INTEGER", true, 1));
        _columnsAlarmTable.put("min", new TableInfo.Column("min", "INTEGER", true, 0));
        _columnsAlarmTable.put("hour", new TableInfo.Column("hour", "INTEGER", true, 0));
        _columnsAlarmTable.put("timeInMillis", new TableInfo.Column("timeInMillis", "INTEGER", true, 0));
        _columnsAlarmTable.put("enabled", new TableInfo.Column("enabled", "INTEGER", true, 0));
        _columnsAlarmTable.put("daysToRepeat", new TableInfo.Column("daysToRepeat", "TEXT", true, 0));
        _columnsAlarmTable.put("isRepeating", new TableInfo.Column("isRepeating", "INTEGER", true, 0));
        _columnsAlarmTable.put("vibration", new TableInfo.Column("vibration", "INTEGER", true, 0));
        _columnsAlarmTable.put("isShutdownDefault", new TableInfo.Column("isShutdownDefault", "INTEGER", true, 0));
        _columnsAlarmTable.put("isShutdownShake", new TableInfo.Column("isShutdownShake", "INTEGER", true, 0));
        _columnsAlarmTable.put("shakeCount", new TableInfo.Column("shakeCount", "INTEGER", true, 0));
        _columnsAlarmTable.put("isShutdownGame", new TableInfo.Column("isShutdownGame", "INTEGER", true, 0));
        _columnsAlarmTable.put("gameLevel", new TableInfo.Column("gameLevel", "INTEGER", true, 0));
        _columnsAlarmTable.put("gameTime", new TableInfo.Column("gameTime", "INTEGER", true, 0));
        _columnsAlarmTable.put("description", new TableInfo.Column("description", "TEXT", false, 0));
        _columnsAlarmTable.put("sound", new TableInfo.Column("sound", "TEXT", false, 0));
        _columnsAlarmTable.put("volume", new TableInfo.Column("volume", "INTEGER", true, 0));
        _columnsAlarmTable.put("allowedSkippingTimes", new TableInfo.Column("allowedSkippingTimes", "INTEGER", true, 0));
        _columnsAlarmTable.put("skippingDelay", new TableInfo.Column("skippingDelay", "INTEGER", true, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysAlarmTable = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesAlarmTable = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoAlarmTable = new TableInfo("alarm_table", _columnsAlarmTable, _foreignKeysAlarmTable, _indicesAlarmTable);
        final TableInfo _existingAlarmTable = TableInfo.read(_db, "alarm_table");
        if (! _infoAlarmTable.equals(_existingAlarmTable)) {
          throw new IllegalStateException("Migration didn't properly handle alarm_table(com.michael.alarmclock.data.alarm.Alarm).\n"
                  + " Expected:\n" + _infoAlarmTable + "\n"
                  + " Found:\n" + _existingAlarmTable);
        }
      }
    }, "395ce87679cc3ab0f86eec062601c36f", "e6ac8b2b788f807900d471ac039dc249");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    return new InvalidationTracker(this, "alarm_table");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    try {
      super.beginTransaction();
      _db.execSQL("DELETE FROM `alarm_table`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  public AlarmDao alarmDao() {
    if (_alarmDao != null) {
      return _alarmDao;
    } else {
      synchronized(this) {
        if(_alarmDao == null) {
          _alarmDao = new AlarmDao_Impl(this);
        }
        return _alarmDao;
      }
    }
  }
}
