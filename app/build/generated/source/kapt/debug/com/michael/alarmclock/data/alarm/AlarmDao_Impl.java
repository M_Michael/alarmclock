package com.michael.alarmclock.data.alarm;

import android.database.Cursor;
import androidx.annotation.NonNull;
import androidx.lifecycle.ComputableLiveData;
import androidx.lifecycle.LiveData;
import androidx.room.EntityDeletionOrUpdateAdapter;
import androidx.room.EntityInsertionAdapter;
import androidx.room.InvalidationTracker.Observer;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.michael.alarmclock.alarm.Converters;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@SuppressWarnings("unchecked")
public final class AlarmDao_Impl implements AlarmDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfAlarm;

  private final Converters __converters = new Converters();

  private final EntityDeletionOrUpdateAdapter __deletionAdapterOfAlarm;

  public AlarmDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfAlarm = new EntityInsertionAdapter<Alarm>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `alarm_table`(`id`,`min`,`hour`,`timeInMillis`,`enabled`,`daysToRepeat`,`isRepeating`,`vibration`,`isShutdownDefault`,`isShutdownShake`,`shakeCount`,`isShutdownGame`,`gameLevel`,`gameTime`,`description`,`sound`,`volume`,`allowedSkippingTimes`,`skippingDelay`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Alarm value) {
        stmt.bindLong(1, value.getId());
        stmt.bindLong(2, value.getMin());
        stmt.bindLong(3, value.getHour());
        stmt.bindLong(4, value.getTimeInMillis());
        final int _tmp;
        _tmp = value.getEnabled() ? 1 : 0;
        stmt.bindLong(5, _tmp);
        final String _tmp_1;
        _tmp_1 = __converters.fromArrayList(value.getDaysToRepeat());
        if (_tmp_1 == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, _tmp_1);
        }
        final int _tmp_2;
        _tmp_2 = value.isRepeating() ? 1 : 0;
        stmt.bindLong(7, _tmp_2);
        final int _tmp_3;
        _tmp_3 = value.getVibration() ? 1 : 0;
        stmt.bindLong(8, _tmp_3);
        final int _tmp_4;
        _tmp_4 = value.isShutdownDefault() ? 1 : 0;
        stmt.bindLong(9, _tmp_4);
        final int _tmp_5;
        _tmp_5 = value.isShutdownShake() ? 1 : 0;
        stmt.bindLong(10, _tmp_5);
        stmt.bindLong(11, value.getShakeCount());
        final int _tmp_6;
        _tmp_6 = value.isShutdownGame() ? 1 : 0;
        stmt.bindLong(12, _tmp_6);
        stmt.bindLong(13, value.getGameLevel());
        stmt.bindLong(14, value.getGameTime());
        if (value.getDescription() == null) {
          stmt.bindNull(15);
        } else {
          stmt.bindString(15, value.getDescription());
        }
        if (value.getSound() == null) {
          stmt.bindNull(16);
        } else {
          stmt.bindString(16, value.getSound());
        }
        stmt.bindLong(17, value.getVolume());
        stmt.bindLong(18, value.getAllowedSkippingTimes());
        stmt.bindLong(19, value.getSkippingDelay());
      }
    };
    this.__deletionAdapterOfAlarm = new EntityDeletionOrUpdateAdapter<Alarm>(__db) {
      @Override
      public String createQuery() {
        return "DELETE FROM `alarm_table` WHERE `id` = ?";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Alarm value) {
        stmt.bindLong(1, value.getId());
      }
    };
  }

  @Override
  public void insert(Alarm alarm) {
    __db.beginTransaction();
    try {
      __insertionAdapterOfAlarm.insert(alarm);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void delete(Alarm alarm) {
    __db.beginTransaction();
    try {
      __deletionAdapterOfAlarm.handle(alarm);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public LiveData<List<Alarm>> getAllLiveData() {
    final String _sql = "SELECT * FROM alarm_table";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return new ComputableLiveData<List<Alarm>>(__db.getQueryExecutor()) {
      private Observer _observer;

      @Override
      protected List<Alarm> compute() {
        if (_observer == null) {
          _observer = new Observer("alarm_table") {
            @Override
            public void onInvalidated(@NonNull Set<String> tables) {
              invalidate();
            }
          };
          __db.getInvalidationTracker().addWeakObserver(_observer);
        }
        final Cursor _cursor = __db.query(_statement);
        try {
          final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
          final int _cursorIndexOfMin = _cursor.getColumnIndexOrThrow("min");
          final int _cursorIndexOfHour = _cursor.getColumnIndexOrThrow("hour");
          final int _cursorIndexOfTimeInMillis = _cursor.getColumnIndexOrThrow("timeInMillis");
          final int _cursorIndexOfEnabled = _cursor.getColumnIndexOrThrow("enabled");
          final int _cursorIndexOfDaysToRepeat = _cursor.getColumnIndexOrThrow("daysToRepeat");
          final int _cursorIndexOfIsRepeating = _cursor.getColumnIndexOrThrow("isRepeating");
          final int _cursorIndexOfVibration = _cursor.getColumnIndexOrThrow("vibration");
          final int _cursorIndexOfIsShutdownDefault = _cursor.getColumnIndexOrThrow("isShutdownDefault");
          final int _cursorIndexOfIsShutdownShake = _cursor.getColumnIndexOrThrow("isShutdownShake");
          final int _cursorIndexOfShakeCount = _cursor.getColumnIndexOrThrow("shakeCount");
          final int _cursorIndexOfIsShutdownGame = _cursor.getColumnIndexOrThrow("isShutdownGame");
          final int _cursorIndexOfGameLevel = _cursor.getColumnIndexOrThrow("gameLevel");
          final int _cursorIndexOfGameTime = _cursor.getColumnIndexOrThrow("gameTime");
          final int _cursorIndexOfDescription = _cursor.getColumnIndexOrThrow("description");
          final int _cursorIndexOfSound = _cursor.getColumnIndexOrThrow("sound");
          final int _cursorIndexOfVolume = _cursor.getColumnIndexOrThrow("volume");
          final int _cursorIndexOfAllowedSkippingTimes = _cursor.getColumnIndexOrThrow("allowedSkippingTimes");
          final int _cursorIndexOfSkippingDelay = _cursor.getColumnIndexOrThrow("skippingDelay");
          final List<Alarm> _result = new ArrayList<Alarm>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final Alarm _item;
            _item = new Alarm();
            final int _tmpId;
            _tmpId = _cursor.getInt(_cursorIndexOfId);
            _item.setId(_tmpId);
            final int _tmpMin;
            _tmpMin = _cursor.getInt(_cursorIndexOfMin);
            _item.setMin(_tmpMin);
            final int _tmpHour;
            _tmpHour = _cursor.getInt(_cursorIndexOfHour);
            _item.setHour(_tmpHour);
            final long _tmpTimeInMillis;
            _tmpTimeInMillis = _cursor.getLong(_cursorIndexOfTimeInMillis);
            _item.setTimeInMillis(_tmpTimeInMillis);
            final boolean _tmpEnabled;
            final int _tmp;
            _tmp = _cursor.getInt(_cursorIndexOfEnabled);
            _tmpEnabled = _tmp != 0;
            _item.setEnabled(_tmpEnabled);
            final ArrayList<Integer> _tmpDaysToRepeat;
            final String _tmp_1;
            _tmp_1 = _cursor.getString(_cursorIndexOfDaysToRepeat);
            _tmpDaysToRepeat = __converters.fromString(_tmp_1);
            _item.setDaysToRepeat(_tmpDaysToRepeat);
            final boolean _tmpIsRepeating;
            final int _tmp_2;
            _tmp_2 = _cursor.getInt(_cursorIndexOfIsRepeating);
            _tmpIsRepeating = _tmp_2 != 0;
            _item.setRepeating(_tmpIsRepeating);
            final boolean _tmpVibration;
            final int _tmp_3;
            _tmp_3 = _cursor.getInt(_cursorIndexOfVibration);
            _tmpVibration = _tmp_3 != 0;
            _item.setVibration(_tmpVibration);
            final boolean _tmpIsShutdownDefault;
            final int _tmp_4;
            _tmp_4 = _cursor.getInt(_cursorIndexOfIsShutdownDefault);
            _tmpIsShutdownDefault = _tmp_4 != 0;
            _item.setShutdownDefault(_tmpIsShutdownDefault);
            final boolean _tmpIsShutdownShake;
            final int _tmp_5;
            _tmp_5 = _cursor.getInt(_cursorIndexOfIsShutdownShake);
            _tmpIsShutdownShake = _tmp_5 != 0;
            _item.setShutdownShake(_tmpIsShutdownShake);
            final int _tmpShakeCount;
            _tmpShakeCount = _cursor.getInt(_cursorIndexOfShakeCount);
            _item.setShakeCount(_tmpShakeCount);
            final boolean _tmpIsShutdownGame;
            final int _tmp_6;
            _tmp_6 = _cursor.getInt(_cursorIndexOfIsShutdownGame);
            _tmpIsShutdownGame = _tmp_6 != 0;
            _item.setShutdownGame(_tmpIsShutdownGame);
            final int _tmpGameLevel;
            _tmpGameLevel = _cursor.getInt(_cursorIndexOfGameLevel);
            _item.setGameLevel(_tmpGameLevel);
            final long _tmpGameTime;
            _tmpGameTime = _cursor.getLong(_cursorIndexOfGameTime);
            _item.setGameTime(_tmpGameTime);
            final String _tmpDescription;
            _tmpDescription = _cursor.getString(_cursorIndexOfDescription);
            _item.setDescription(_tmpDescription);
            final String _tmpSound;
            _tmpSound = _cursor.getString(_cursorIndexOfSound);
            _item.setSound(_tmpSound);
            final int _tmpVolume;
            _tmpVolume = _cursor.getInt(_cursorIndexOfVolume);
            _item.setVolume(_tmpVolume);
            final int _tmpAllowedSkippingTimes;
            _tmpAllowedSkippingTimes = _cursor.getInt(_cursorIndexOfAllowedSkippingTimes);
            _item.setAllowedSkippingTimes(_tmpAllowedSkippingTimes);
            final long _tmpSkippingDelay;
            _tmpSkippingDelay = _cursor.getLong(_cursorIndexOfSkippingDelay);
            _item.setSkippingDelay(_tmpSkippingDelay);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    }.getLiveData();
  }

  @Override
  public List<Alarm> getAll() {
    final String _sql = "SELECT * FROM alarm_table";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
      final int _cursorIndexOfMin = _cursor.getColumnIndexOrThrow("min");
      final int _cursorIndexOfHour = _cursor.getColumnIndexOrThrow("hour");
      final int _cursorIndexOfTimeInMillis = _cursor.getColumnIndexOrThrow("timeInMillis");
      final int _cursorIndexOfEnabled = _cursor.getColumnIndexOrThrow("enabled");
      final int _cursorIndexOfDaysToRepeat = _cursor.getColumnIndexOrThrow("daysToRepeat");
      final int _cursorIndexOfIsRepeating = _cursor.getColumnIndexOrThrow("isRepeating");
      final int _cursorIndexOfVibration = _cursor.getColumnIndexOrThrow("vibration");
      final int _cursorIndexOfIsShutdownDefault = _cursor.getColumnIndexOrThrow("isShutdownDefault");
      final int _cursorIndexOfIsShutdownShake = _cursor.getColumnIndexOrThrow("isShutdownShake");
      final int _cursorIndexOfShakeCount = _cursor.getColumnIndexOrThrow("shakeCount");
      final int _cursorIndexOfIsShutdownGame = _cursor.getColumnIndexOrThrow("isShutdownGame");
      final int _cursorIndexOfGameLevel = _cursor.getColumnIndexOrThrow("gameLevel");
      final int _cursorIndexOfGameTime = _cursor.getColumnIndexOrThrow("gameTime");
      final int _cursorIndexOfDescription = _cursor.getColumnIndexOrThrow("description");
      final int _cursorIndexOfSound = _cursor.getColumnIndexOrThrow("sound");
      final int _cursorIndexOfVolume = _cursor.getColumnIndexOrThrow("volume");
      final int _cursorIndexOfAllowedSkippingTimes = _cursor.getColumnIndexOrThrow("allowedSkippingTimes");
      final int _cursorIndexOfSkippingDelay = _cursor.getColumnIndexOrThrow("skippingDelay");
      final List<Alarm> _result = new ArrayList<Alarm>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final Alarm _item;
        _item = new Alarm();
        final int _tmpId;
        _tmpId = _cursor.getInt(_cursorIndexOfId);
        _item.setId(_tmpId);
        final int _tmpMin;
        _tmpMin = _cursor.getInt(_cursorIndexOfMin);
        _item.setMin(_tmpMin);
        final int _tmpHour;
        _tmpHour = _cursor.getInt(_cursorIndexOfHour);
        _item.setHour(_tmpHour);
        final long _tmpTimeInMillis;
        _tmpTimeInMillis = _cursor.getLong(_cursorIndexOfTimeInMillis);
        _item.setTimeInMillis(_tmpTimeInMillis);
        final boolean _tmpEnabled;
        final int _tmp;
        _tmp = _cursor.getInt(_cursorIndexOfEnabled);
        _tmpEnabled = _tmp != 0;
        _item.setEnabled(_tmpEnabled);
        final ArrayList<Integer> _tmpDaysToRepeat;
        final String _tmp_1;
        _tmp_1 = _cursor.getString(_cursorIndexOfDaysToRepeat);
        _tmpDaysToRepeat = __converters.fromString(_tmp_1);
        _item.setDaysToRepeat(_tmpDaysToRepeat);
        final boolean _tmpIsRepeating;
        final int _tmp_2;
        _tmp_2 = _cursor.getInt(_cursorIndexOfIsRepeating);
        _tmpIsRepeating = _tmp_2 != 0;
        _item.setRepeating(_tmpIsRepeating);
        final boolean _tmpVibration;
        final int _tmp_3;
        _tmp_3 = _cursor.getInt(_cursorIndexOfVibration);
        _tmpVibration = _tmp_3 != 0;
        _item.setVibration(_tmpVibration);
        final boolean _tmpIsShutdownDefault;
        final int _tmp_4;
        _tmp_4 = _cursor.getInt(_cursorIndexOfIsShutdownDefault);
        _tmpIsShutdownDefault = _tmp_4 != 0;
        _item.setShutdownDefault(_tmpIsShutdownDefault);
        final boolean _tmpIsShutdownShake;
        final int _tmp_5;
        _tmp_5 = _cursor.getInt(_cursorIndexOfIsShutdownShake);
        _tmpIsShutdownShake = _tmp_5 != 0;
        _item.setShutdownShake(_tmpIsShutdownShake);
        final int _tmpShakeCount;
        _tmpShakeCount = _cursor.getInt(_cursorIndexOfShakeCount);
        _item.setShakeCount(_tmpShakeCount);
        final boolean _tmpIsShutdownGame;
        final int _tmp_6;
        _tmp_6 = _cursor.getInt(_cursorIndexOfIsShutdownGame);
        _tmpIsShutdownGame = _tmp_6 != 0;
        _item.setShutdownGame(_tmpIsShutdownGame);
        final int _tmpGameLevel;
        _tmpGameLevel = _cursor.getInt(_cursorIndexOfGameLevel);
        _item.setGameLevel(_tmpGameLevel);
        final long _tmpGameTime;
        _tmpGameTime = _cursor.getLong(_cursorIndexOfGameTime);
        _item.setGameTime(_tmpGameTime);
        final String _tmpDescription;
        _tmpDescription = _cursor.getString(_cursorIndexOfDescription);
        _item.setDescription(_tmpDescription);
        final String _tmpSound;
        _tmpSound = _cursor.getString(_cursorIndexOfSound);
        _item.setSound(_tmpSound);
        final int _tmpVolume;
        _tmpVolume = _cursor.getInt(_cursorIndexOfVolume);
        _item.setVolume(_tmpVolume);
        final int _tmpAllowedSkippingTimes;
        _tmpAllowedSkippingTimes = _cursor.getInt(_cursorIndexOfAllowedSkippingTimes);
        _item.setAllowedSkippingTimes(_tmpAllowedSkippingTimes);
        final long _tmpSkippingDelay;
        _tmpSkippingDelay = _cursor.getLong(_cursorIndexOfSkippingDelay);
        _item.setSkippingDelay(_tmpSkippingDelay);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public Alarm getById(int id) {
    final String _sql = "SELECT * FROM alarm_table WHERE id = ? LIMIT 1";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, id);
    final Cursor _cursor = __db.query(_statement);
    try {
      final int _cursorIndexOfId = _cursor.getColumnIndexOrThrow("id");
      final int _cursorIndexOfMin = _cursor.getColumnIndexOrThrow("min");
      final int _cursorIndexOfHour = _cursor.getColumnIndexOrThrow("hour");
      final int _cursorIndexOfTimeInMillis = _cursor.getColumnIndexOrThrow("timeInMillis");
      final int _cursorIndexOfEnabled = _cursor.getColumnIndexOrThrow("enabled");
      final int _cursorIndexOfDaysToRepeat = _cursor.getColumnIndexOrThrow("daysToRepeat");
      final int _cursorIndexOfIsRepeating = _cursor.getColumnIndexOrThrow("isRepeating");
      final int _cursorIndexOfVibration = _cursor.getColumnIndexOrThrow("vibration");
      final int _cursorIndexOfIsShutdownDefault = _cursor.getColumnIndexOrThrow("isShutdownDefault");
      final int _cursorIndexOfIsShutdownShake = _cursor.getColumnIndexOrThrow("isShutdownShake");
      final int _cursorIndexOfShakeCount = _cursor.getColumnIndexOrThrow("shakeCount");
      final int _cursorIndexOfIsShutdownGame = _cursor.getColumnIndexOrThrow("isShutdownGame");
      final int _cursorIndexOfGameLevel = _cursor.getColumnIndexOrThrow("gameLevel");
      final int _cursorIndexOfGameTime = _cursor.getColumnIndexOrThrow("gameTime");
      final int _cursorIndexOfDescription = _cursor.getColumnIndexOrThrow("description");
      final int _cursorIndexOfSound = _cursor.getColumnIndexOrThrow("sound");
      final int _cursorIndexOfVolume = _cursor.getColumnIndexOrThrow("volume");
      final int _cursorIndexOfAllowedSkippingTimes = _cursor.getColumnIndexOrThrow("allowedSkippingTimes");
      final int _cursorIndexOfSkippingDelay = _cursor.getColumnIndexOrThrow("skippingDelay");
      final Alarm _result;
      if(_cursor.moveToFirst()) {
        _result = new Alarm();
        final int _tmpId;
        _tmpId = _cursor.getInt(_cursorIndexOfId);
        _result.setId(_tmpId);
        final int _tmpMin;
        _tmpMin = _cursor.getInt(_cursorIndexOfMin);
        _result.setMin(_tmpMin);
        final int _tmpHour;
        _tmpHour = _cursor.getInt(_cursorIndexOfHour);
        _result.setHour(_tmpHour);
        final long _tmpTimeInMillis;
        _tmpTimeInMillis = _cursor.getLong(_cursorIndexOfTimeInMillis);
        _result.setTimeInMillis(_tmpTimeInMillis);
        final boolean _tmpEnabled;
        final int _tmp;
        _tmp = _cursor.getInt(_cursorIndexOfEnabled);
        _tmpEnabled = _tmp != 0;
        _result.setEnabled(_tmpEnabled);
        final ArrayList<Integer> _tmpDaysToRepeat;
        final String _tmp_1;
        _tmp_1 = _cursor.getString(_cursorIndexOfDaysToRepeat);
        _tmpDaysToRepeat = __converters.fromString(_tmp_1);
        _result.setDaysToRepeat(_tmpDaysToRepeat);
        final boolean _tmpIsRepeating;
        final int _tmp_2;
        _tmp_2 = _cursor.getInt(_cursorIndexOfIsRepeating);
        _tmpIsRepeating = _tmp_2 != 0;
        _result.setRepeating(_tmpIsRepeating);
        final boolean _tmpVibration;
        final int _tmp_3;
        _tmp_3 = _cursor.getInt(_cursorIndexOfVibration);
        _tmpVibration = _tmp_3 != 0;
        _result.setVibration(_tmpVibration);
        final boolean _tmpIsShutdownDefault;
        final int _tmp_4;
        _tmp_4 = _cursor.getInt(_cursorIndexOfIsShutdownDefault);
        _tmpIsShutdownDefault = _tmp_4 != 0;
        _result.setShutdownDefault(_tmpIsShutdownDefault);
        final boolean _tmpIsShutdownShake;
        final int _tmp_5;
        _tmp_5 = _cursor.getInt(_cursorIndexOfIsShutdownShake);
        _tmpIsShutdownShake = _tmp_5 != 0;
        _result.setShutdownShake(_tmpIsShutdownShake);
        final int _tmpShakeCount;
        _tmpShakeCount = _cursor.getInt(_cursorIndexOfShakeCount);
        _result.setShakeCount(_tmpShakeCount);
        final boolean _tmpIsShutdownGame;
        final int _tmp_6;
        _tmp_6 = _cursor.getInt(_cursorIndexOfIsShutdownGame);
        _tmpIsShutdownGame = _tmp_6 != 0;
        _result.setShutdownGame(_tmpIsShutdownGame);
        final int _tmpGameLevel;
        _tmpGameLevel = _cursor.getInt(_cursorIndexOfGameLevel);
        _result.setGameLevel(_tmpGameLevel);
        final long _tmpGameTime;
        _tmpGameTime = _cursor.getLong(_cursorIndexOfGameTime);
        _result.setGameTime(_tmpGameTime);
        final String _tmpDescription;
        _tmpDescription = _cursor.getString(_cursorIndexOfDescription);
        _result.setDescription(_tmpDescription);
        final String _tmpSound;
        _tmpSound = _cursor.getString(_cursorIndexOfSound);
        _result.setSound(_tmpSound);
        final int _tmpVolume;
        _tmpVolume = _cursor.getInt(_cursorIndexOfVolume);
        _result.setVolume(_tmpVolume);
        final int _tmpAllowedSkippingTimes;
        _tmpAllowedSkippingTimes = _cursor.getInt(_cursorIndexOfAllowedSkippingTimes);
        _result.setAllowedSkippingTimes(_tmpAllowedSkippingTimes);
        final long _tmpSkippingDelay;
        _tmpSkippingDelay = _cursor.getLong(_cursorIndexOfSkippingDelay);
        _result.setSkippingDelay(_tmpSkippingDelay);
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
